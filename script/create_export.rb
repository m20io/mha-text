export = ExportSingleCount.generate_csv
File.open("export/export_single_#{Time.now.strftime("%y_%m_%d")
}.csv",'w') {|f| f.write export }


export = ExportDifferences.generate_csv
File.open("export/export_diff_#{Time.now.strftime("%y_%m_%d")
}.csv",'w') {|f| f.write export }