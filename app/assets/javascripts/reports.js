google.load("visualization", "1", {
    packages : ["corechart"]
});
google.setOnLoadCallback(drawChart);
function drawChart() {
    if ($('#chart').length > 0 && $('#chart').data() != null) {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Kombination');
        data.addColumn('number', 'nicht gestressed');
        data.addColumn('number', 'gestressed');
        
        $.each($('#chart').data().labels, function(index,value) {
            data.addRow([$('#chart').data().labels[index], 
                $('#chart').data().dvu[index], 
                $('#chart').data().dvs[index]]);    
        });
        
        var options = {
            width : 800,
            height : 300,
            title : 'Time Differences'
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart'));
        chart.draw(data, options);
    }
}