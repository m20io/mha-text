/**
 * KeyListener to track all key presses.
 */

 var keyEventArray;
 var first_time = true;
 jQuery(document).ready(function() {

	function resetKeyEventArray() {
		keyEventArray = new Array();
	 }
	 
	 function startMonitor() {
	    $('body').keydown(function(event) {
		    var keyEvent = new Object();
		    keyEvent["type"] = "keydown";
		    keyEvent['userId'] = $('#user_id').val();
		    keyEvent["which"] = event.which;
		    keyEvent["ctrlKey"] = event.ctrlKey;
		    keyEvent["altKey"] = event.altKey;
		    keyEvent["shiftKey"] = event.shiftKey;
		    keyEvent["target"] = event.target.nodeName + "_" + event.target.id;
		    keyEvent["timeStamp"] = event.timeStamp;
		    keyEventArray.push(keyEvent)
	    });

		
	
	    $('body').keyup(function(event) {

		    var keyEvent = new Object();
		    keyEvent["type"] = "keyup";
		    keyEvent['userId'] = $('#user_id').val();
		    keyEvent["which"] = event.which;
		    keyEvent["ctrlKey"] = event.ctrlKey;
		    keyEvent["altKey"] = event.altKey;
		    keyEvent["shiftKey"] = event.shiftKey;
		    keyEvent["target"] = event.target.nodeName + "_" + event.target.id;
		    keyEvent["timeStamp"] = event.timeStamp;
		    keyEventArray.push(keyEvent)
	    });
	
	    $(document).everyTime(5000,function() {
		    sendDataToServer();
	    });
	
    };
	function sendDataToServer()  {
		/**
		 * Send array to Server
		 */
		$.post( '/key_strokes/create', JSON.stringify(keyEventArray), function(data, textStatus, jqXHR) {
			$('#message').text((data['message']));
		},
		"JSON"
		);
		resetKeyEventArray();
	};
 	function init() {
 	    if ( $('#input_form').length > 0 && first_time ) {
 	        first_time = false
 	        startMonitor();
     	    createCoutdown();
		    resetKeyEventArray();
		    $('button.button').click(function() {
			    sendDataToServer();
			    $('#input_form').submit();
		    });
		}
    };

    
    function createCoutdown() {
        var fiveMinutsFromNow=new Date();
        fiveMinutsFromNow = new Date( fiveMinutsFromNow.getTime() + 5 * 60 * 1000)
        $('#countdown').countdown({ until: fiveMinutsFromNow, compact: true, format: 'MS', description: '', onExpiry: submitInputForm });
    };
    
    function submitInputForm() {
        sendDataToServer();  
        $('#input_form').submit();
    };
    init();
});

