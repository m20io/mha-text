module TextSessionsHelper
  def highlight_regexp(text,regexps = [])
    regexps.each do |regexp|
      text.gsub!(Regexp.new(regexp), "<span class='match'>\\0</span>")
    end

    text
  end
end
