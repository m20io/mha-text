module ReportHelper
  def to_letter(number)
    KeyStroke.keymap.invert[number.to_s]
  end
  
  def to_nubmer(char)
    KeyStroke.keymap[char.to_s]
  end
  
  def encode_chart_data(keys, list_unstressed, list_stressed)
    labels = []
    dvu = []
    dvs = []
    
    keys.each do |key|
       unless list_unstressed[key].present? && list_unstressed[key][:count] > 2 && 
              list_stressed[key].present? && list_stressed[key][:count] > 2
         next
      end
      
      labels << key
      dvu << ("%.2f" % list_unstressed[key][:average]).to_f
      dvs << ("%.2f" % list_stressed[key][:average]).to_f
    end
    
    "data-labels=#{labels.to_json} data-dvu=#{dvu.to_json} data-dvs=#{dvs.to_json}"
  end
end