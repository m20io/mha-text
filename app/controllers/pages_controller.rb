class PagesController < ApplicationController
  skip_before_filter :authenticate
  
  # GET start
  def start
    render :layout => "box"
  end

  def data
    sql = ActiveRecord::Base.connection
    sql.execute "SET autocommit=0";
    sql.begin_db_transaction 
    (1..10000).each do |index|
      sql.execute "INSERT INTO `key_strokes` (`alt_key`, `created_at`, `ctrl_key`, `event_type`, `position`, `shift_key`, `stroked_key`, `target`, `text_session_id`, `time_stamp`, `updated_at`) VALUES (NULL, '#{Time.now.to_s}', NULL, NULL, NULL, NULL, #{index}, NULL, NULL, 1325787107, '#{Time.now.to_s}')"
    end
    sql.commit_db_transaction
  end

  # POST start
  def start_redirect
    find_text_session
    if @text_session.blank? 
      render :start, :layout => "box"
    else 
      if @text_session.final_text_1.blank? && @text_session.final_text_2.blank?
        redirect_to pages_write_text_1_path(:text_session_id => @text_session.id)
      end
      if @text_session.final_text_1.present? && @text_session.final_text_2.blank?
        redirect_to pages_write_text_2_path(:text_session_id => @text_session.id)
      end
      if @text_session.final_text_1.present? && @text_session.final_text_2.present?
        redirect_to pages_finish_path
      end
    end
  end
  
  # GET write_text_1
  def write_text_1
    find_text_session
    redirect_to pages_start_path if @text_session.blank?
  end
  
  # POST write_text_1
  def write_text_1_save
    find_text_session
    @text_session.final_text_1 = params[:input]
    @text_session.save()
    KeyStroke.where(:text_session_id => @text_session.id).where("position IS NULL").update_all(:position => "1")
    redirect_to pages_games_path
  end

  # GET game_start
  def game_start
        
  end
  
  def game_question
    
  end
  
  def game_process_answer
    
  end
  
  def game_finish
    
  end
  
  # GET write_text_2
  def write_text_2
    find_text_session
    redirect_to pages_start_path if @text_session.blank?
  end
  
  # POST write_text_2
  def write_text_2_save
    find_text_session
    @text_session.final_text_2 = params[:input]
    @text_session.save()
    KeyStroke.where(:text_session_id => @text_session.id).where("position IS NULL").update_all(:position => "2")
    redirect_to pages_finish_path
  end

  def finish
    render :layout => "box"
  end

  private 
  
  def find_text_session
    if params[:user_id]
      @text_session = TextSession.find_by_user_id(params[:user_id])
    else
      @text_session = TextSession.find_by_id(params[:text_session_id])
    end
  end
  
end
