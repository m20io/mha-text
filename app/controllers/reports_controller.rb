class ReportsController < ApplicationController

  def show

  end

  def index
    @reports = Report.joins(:text_session).where("key_stroke_counts IS NOT NULL").order("text_sessions.user_id desc")
    respond_to do |format|
      format.html
      format.csv { send_data @reports.generate_csv }
    end
  end

  def show
    @report = Report.find_by_text_session_id(TextSession.find(params[:id]))

    if @report.present?
      @matrix = @report.calculate_average_time_distance_matrix(:unstressed)
      @list_unstressed = @report.calculate_time_distance_list(:unstressed)
      @list_stressed = @report.calculate_time_distance_list(:stressed)
      @keys = @report.key_press_combinations
      @strokes = @report.accrued_key_strokes_with_counts
    else
      redirect_to reports_path
    end
  end
end
