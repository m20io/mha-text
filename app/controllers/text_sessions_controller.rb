class TextSessionsController < ApplicationController
  
  def index
    @text_sessions = TextSession.all
    @text_session_new = TextSession.new
  end
  
  def show
    @text_session = TextSession.find_by_id(params[:id])
    @key_strokes_text_1 = KeyStroke.where(:text_session_id => @text_session).
      where(:position => "1")
    @key_strokes_text_2 = KeyStroke.where(:text_session_id => @text_session).
      where(:position => "2")
  end
  
  def create
    @text_session = TextSession.new(params[:text_session])
    if @text_session.save
      flash[:notice] = "New Text Session created."
    else
      flash[:error] = "Text Session could not be saved."
    end
    redirect_to text_sessions_path
  end
  
  def update
    @text_session = TextSession.find_by_id(params[:id])
    @text_session.update_attributes(params[:text_session])
    if @text_session.save
      flash[:notice] = "Text Session has been updated."
      redirect_to text_sessions_path
    else 
      flash[:notice] = "Text Session has not been updated."
      @text_sessions = TextSession.all
      @text_session_new = TextSession.new
      render :index
    end
  end
  
  def destroy
    @text_session = TextSession.find_by_id params[:id]
    KeyStroke.where(:text_session_id => @text_session.id).delete_all
    @text_session.delete 
    flash[:notice] = "Text Session has been deleted."
    redirect_to text_sessions_path
  end
  
end
