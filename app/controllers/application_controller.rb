class ApplicationController < ActionController::Base
  protect_from_forgery
  USERNAME, PASSWORD = "mha-text", "d199a0aef8e3fc554765afc783642366" # f59aDiak#

  before_filter :authenticate

private

  def authenticate
    authenticate_or_request_with_http_basic do |username, password|
      username == USERNAME && Digest::MD5.hexdigest(password) == PASSWORD
    end
  end

end

