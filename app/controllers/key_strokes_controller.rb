class KeyStrokesController < ApplicationController

  skip_before_filter :authenticate

  # POST /key_strokes/new
  def create 
    events = ActiveSupport::JSON.decode(request.body.read.to_s)
    events.each do |event|
      key_stroke = KeyStroke.new
      key_stroke.text_session_id = event['userId']
      key_stroke.event_type = event['type']
      key_stroke.stroked_key = event['which']
      key_stroke.ctrl_key = event['ctrlKey']
      key_stroke.alt_key = event['altKey']
      key_stroke.shift_key = event['shiftKey']
      key_stroke.target = event['target']
      key_stroke.time_stamp = event['timeStamp']
      if(key_stroke.valid?)
        key_stroke.save
      else 
        raise "Unvalid Data for KeyStroke class"
      end
    end
      
    respond_to do |format| 
      # no other formats
      result = Hash.new
      result["message"] = "#{events.length} events has been saved"
      format.json { render :json => result }
    end
  end

end
