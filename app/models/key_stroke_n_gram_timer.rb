class KeyStrokeNGramTimer < NGramBaseTimer

  def self.analyse(text_session, report)

    kst = KeyStrokeNGramTimer.new(text_session, report)
    hash = {}
    (TextSession.common_bi_grams + TextSession.common_tri_grams).each do |ngram|
      hash.merge!({
        "ngram_#{ngram}_count_unstressed".to_sym => kst.count_key_stroke_sequences(ngram, 1),
        "ngram_#{ngram}_count_stressed".to_sym => kst.count_key_stroke_sequences(ngram, 2),
        "ngram_#{ngram}_p2r_unstressed".to_sym => kst.measure_ngram_p2r(ngram, 1) /
          kst.count_key_stroke_sequences(ngram, 1),
        "ngram_#{ngram}_p2r_stressed".to_sym => kst.measure_ngram_p2r(ngram, 2) /
          kst.count_key_stroke_sequences(ngram, 2),
        "ngram_#{ngram}_r2p_unstressed".to_sym => kst.measure_ngram_p2r(ngram, 1) /
          kst.count_key_stroke_sequences(ngram, 1),
        "ngram_#{ngram}_r2p_stressed".to_sym => kst.measure_ngram_p2r(ngram, 2) /
          kst.count_key_stroke_sequences(ngram, 1)
      })
    end
    report.ngram_data.merge!(hash)

    # print "calculate for single ngrams done - #{Time.now - time}\n"
    # time = Time.now
    # Digraphs

    report.ngram_data[:common_digram_p2r_unstressed] = kst.measure_multi_ngram_p2r(TextSession.common_bi_grams,1) /
      kst.count_multi_ngram(TextSession.common_bi_grams, 1)
    report.ngram_data[:common_digram_p2r_stressed] = kst.measure_multi_ngram_p2r(TextSession.common_bi_grams,2) /
      kst.count_multi_ngram(TextSession.common_bi_grams, 2)

    report.ngram_data[:common_digram_r2p_unstressed] = kst.measure_multi_ngram_r2p(TextSession.common_bi_grams,1) /
      kst.count_multi_ngram(TextSession.common_bi_grams, 1)
    report.ngram_data[:common_digram_r2p_stressed] = kst.measure_multi_ngram_r2p(TextSession.common_bi_grams,2) /
      kst.count_multi_ngram(TextSession.common_bi_grams, 2)

    report.ngram_data[:common_digram_count_unstressed] = kst.count_multi_ngram(TextSession.common_bi_grams, 1)
    report.ngram_data[:common_digram_count_stressed] = kst.count_multi_ngram(TextSession.common_bi_grams, 2)


    report.ngram_data[:common_inter_digram_p2r_unstressed] = kst.measure_multi_ngram_p2r(TextSession.inter_hand_bi_grams, 1) /
      kst.count_multi_ngram(TextSession.inter_hand_bi_grams, 1)
    report.ngram_data[:common_inter_digram_p2r_stressed] = kst.measure_multi_ngram_p2r(TextSession.inter_hand_bi_grams, 2) /
      kst.count_multi_ngram(TextSession.inter_hand_bi_grams, 2)

    report.ngram_data[:common_inter_digram_r2p_unstressed] = kst.measure_multi_ngram_r2p(TextSession.inter_hand_bi_grams,1) /
      kst.count_multi_ngram(TextSession.inter_hand_bi_grams, 1)
    report.ngram_data[:common_inter_digram_r2p_stressed] = kst.measure_multi_ngram_r2p(TextSession.inter_hand_bi_grams,2) /
      kst.count_multi_ngram(TextSession.inter_hand_bi_grams, 2)

    report.ngram_data[:common_inter_digram_count_unstressed] = kst.count_multi_ngram(TextSession.inter_hand_bi_grams, 1)
    report.ngram_data[:common_inter_digram_count_stressed] = kst.count_multi_ngram(TextSession.inter_hand_bi_grams, 2)

    report.ngram_data[:common_intra_digram_p2r_unstressed] = kst.measure_multi_ngram_p2r(TextSession.intra_hand_bi_grams,1) /
      kst.count_multi_ngram(TextSession.intra_hand_bi_grams, 1)
    report.ngram_data[:common_intra_digram_p2r_stressed] = kst.measure_multi_ngram_p2r(TextSession.intra_hand_bi_grams,2) /
      kst.count_multi_ngram(TextSession.intra_hand_bi_grams, 2)

    report.ngram_data[:common_intra_digram_r2p_unstressed] = kst.measure_multi_ngram_r2p(TextSession.intra_hand_bi_grams,1) /
      kst.count_multi_ngram(TextSession.intra_hand_bi_grams, 1)
    report.ngram_data[:common_intra_digram_r2p_stressed] = kst.measure_multi_ngram_r2p(TextSession.intra_hand_bi_grams,2) /
      kst.count_multi_ngram(TextSession.intra_hand_bi_grams, 2)

    report.ngram_data[:common_intra_digram_count_unstressed] = kst.count_multi_ngram(TextSession.intra_hand_bi_grams, 1)
    report.ngram_data[:common_intra_digram_count_stressed] = kst.count_multi_ngram(TextSession.intra_hand_bi_grams, 2)

    # print "calculate for bi grams done - #{Time.now - time}\n"
    # time = Time.now
    # Digraph Firsts

    report.ngram_data[:common_digram_p2r_1_unstressed] = kst.measure_multi_ngram_p2r_1(TextSession.common_bi_grams,1) /
      kst.count_multi_ngram(TextSession.common_bi_grams, 1)
    report.ngram_data[:common_digram_p2r_1_stressed] = kst.measure_multi_ngram_p2r_1(TextSession.common_bi_grams,2) /
      kst.count_multi_ngram(TextSession.common_bi_grams, 2)

    report.ngram_data[:common_inter_digram_p2r_1_unstressed] = kst.measure_multi_ngram_p2r_1(TextSession.inter_hand_bi_grams,1) /
      kst.count_multi_ngram(TextSession.inter_hand_bi_grams, 1)
    report.ngram_data[:common_inter_digram_p2r_1_stressed] = kst.measure_multi_ngram_p2r_1(TextSession.inter_hand_bi_grams,2) /
      kst.count_multi_ngram(TextSession.inter_hand_bi_grams, 2)

    report.ngram_data[:common_intra_digram_p2r_1_unstressed] = kst.measure_multi_ngram_p2r_1(TextSession.intra_hand_bi_grams,1) /
      kst.count_multi_ngram(TextSession.intra_hand_bi_grams, 1)
    report.ngram_data[:common_intra_digram_p2r_1_stressed] = kst.measure_multi_ngram_p2r_1(TextSession.intra_hand_bi_grams,2) /
      kst.count_multi_ngram(TextSession.intra_hand_bi_grams, 2)


    ('a'..'z').to_a.each do |letter|
      bi_grams = TextSession.all_bi_grams.select{|big| big[0] == letter}
      report.ngram_data["bi_gram_#{letter}_p2r_1_unstressed".to_sym] = kst.measure_multi_ngram_p2r_1(bi_grams, 1) /
        kst.count_multi_ngram(bi_grams, 1)
      report.ngram_data["bi_gram_#{letter}_p2r_1_stressed".to_sym] = kst.measure_multi_ngram_p2r_1(bi_grams, 2) /
        kst.count_multi_ngram(bi_grams, 2)
      report.ngram_data["bi_gram_#{letter}_r2p_1_unstressed".to_sym] = kst.measure_multi_ngram_r2p_1(bi_grams,1) /
        kst.count_multi_ngram(bi_grams, 1)
      report.ngram_data["bi_gram_#{letter}_r2p_1_stressed".to_sym] = kst.measure_multi_ngram_r2p_1(bi_grams,2) /
        kst.count_multi_ngram(bi_grams, 2)

      if TextSession.left_hand_letters.include?(letter)
        inter_hand_bi_grams = TextSession.all_bi_grams.select{|big| big[0] == letter \
          && TextSession.right_hand_letters.include?(big[1]) }
        intra_hand_bi_grams = TextSession.all_bi_grams.select{|big| big[0] == letter \
          && TextSession.left_hand_letters.include?(big[1]) }
      else
        inter_hand_bi_grams = TextSession.all_bi_grams.select{|big| big[0] == letter \
          && TextSession.left_hand_letters.include?(big[1]) }
        intra_hand_bi_grams = TextSession.all_bi_grams.select{|big| big[0] == letter \
          && TextSession.right_hand_letters.include?(big[1]) }
      end

      report.ngram_data["bi_gram_#{letter}_inter_hand_p2r_1_unstressed".to_sym] = kst.measure_multi_ngram_p2r_1(inter_hand_bi_grams, 1) /
        kst.count_multi_ngram(inter_hand_bi_grams, 1)
      report.ngram_data["bi_gram_#{letter}_inter_hand_p2r_1_stressed".to_sym] = kst.measure_multi_ngram_p2r_1(inter_hand_bi_grams, 2) /
        kst.count_multi_ngram(inter_hand_bi_grams, 2)
      report.ngram_data["bi_gram_#{letter}_inter_hand_r2p_1_unstressed".to_sym] = kst.measure_multi_ngram_r2p_1(inter_hand_bi_grams,1) /
        kst.count_multi_ngram(inter_hand_bi_grams, 1)
      report.ngram_data["bi_gram_#{letter}_inter_hand_r2p_1_stressed".to_sym] = kst.measure_multi_ngram_r2p_1(inter_hand_bi_grams,2) /
        kst.count_multi_ngram(inter_hand_bi_grams, 2)

      report.ngram_data["bi_gram_#{letter}_intra_hand_p2r_1_unstressed".to_sym] = kst.measure_multi_ngram_p2r_1(intra_hand_bi_grams, 1) /
        kst.count_multi_ngram(intra_hand_bi_grams, 1)
      report.ngram_data["bi_gram_#{letter}_intra_hand_p2r_1_stressed".to_sym] = kst.measure_multi_ngram_p2r_1(intra_hand_bi_grams, 2) /
        kst.count_multi_ngram(intra_hand_bi_grams, 2)
      report.ngram_data["bi_gram_#{letter}_intra_hand_r2p_1_unstressed".to_sym] = kst.measure_multi_ngram_r2p_1(intra_hand_bi_grams,1) /
        kst.count_multi_ngram(intra_hand_bi_grams, 1)
      report.ngram_data["bi_gram_#{letter}_intra_hand_r2p_1_stressed".to_sym] = kst.measure_multi_ngram_r2p_1(intra_hand_bi_grams,2) /
        kst.count_multi_ngram(intra_hand_bi_grams, 2)
    end
    # print "calculate for bi grams firsts done- #{Time.now - time}\n"
    # time = Time.now

    # Digraph Uncommon

    report.ngram_data[:uncommon_digram_p2r_unstressed] = kst.measure_multi_ngram_p2r(TextSession.uncommon_bi_grams,1) /
      kst.count_multi_ngram(TextSession.uncommon_bi_grams,1)
    report.ngram_data[:uncommon_digram_p2r_stressed] = kst.measure_multi_ngram_p2r(TextSession.uncommon_bi_grams,2) /
      kst.count_multi_ngram(TextSession.uncommon_bi_grams,2)
    report.ngram_data[:uncommon_digram_r2p_unstressed] = kst.measure_multi_ngram_r2p(TextSession.uncommon_bi_grams,1) /
      kst.count_multi_ngram(TextSession.uncommon_bi_grams,1)
    report.ngram_data[:uncommon_digram_r2p_stressed] = kst.measure_multi_ngram_p2r(TextSession.uncommon_bi_grams,2) /
      kst.count_multi_ngram(TextSession.uncommon_bi_grams,2)
    report.ngram_data[:uncommon_digram_count_unstressed] = kst.count_multi_ngram(TextSession.uncommon_bi_grams,1)
    report.ngram_data[:uncommon_digram_count_stressed] = kst.count_multi_ngram(TextSession.uncommon_bi_grams,2)


    report.ngram_data[:uncommon_inter_digram_p2r_unstressed] = kst.measure_multi_ngram_p2r(TextSession.uncommon_inter_hand_bi_grams,1) /
      kst.count_multi_ngram(TextSession.uncommon_inter_hand_bi_grams,1)
    report.ngram_data[:uncommon_inter_digram_p2r_stressed] = kst.measure_multi_ngram_p2r(TextSession.uncommon_inter_hand_bi_grams,2) /
      kst.count_multi_ngram(TextSession.uncommon_inter_hand_bi_grams,2)
    report.ngram_data[:uncommon_inter_digram_r2p_unstressed] = kst.measure_multi_ngram_r2p(TextSession.uncommon_inter_hand_bi_grams,1) /
      kst.count_multi_ngram(TextSession.uncommon_inter_hand_bi_grams,1)
    report.ngram_data[:uncommon_inter_digram_r2p_stressed] = kst.measure_multi_ngram_r2p(TextSession.uncommon_inter_hand_bi_grams,2) /
      kst.count_multi_ngram(TextSession.uncommon_inter_hand_bi_grams,2)
    report.ngram_data[:uncommon_inter_digram_count_unstressed] = kst.count_multi_ngram(TextSession.uncommon_inter_hand_bi_grams,1)
    report.ngram_data[:uncommon_inter_digram_count_stressed] = kst.count_multi_ngram(TextSession.uncommon_inter_hand_bi_grams,2)

    report.ngram_data[:uncommon_intra_digram_p2r_unstressed] = kst.measure_multi_ngram_p2r(TextSession.uncommon_intra_hand_bi_grams,1) /
      kst.count_multi_ngram(TextSession.uncommon_intra_hand_bi_grams,1)
    report.ngram_data[:uncommon_intra_digram_p2r_stressed] = kst.measure_multi_ngram_p2r(TextSession.uncommon_intra_hand_bi_grams,2) /
      kst.count_multi_ngram(TextSession.uncommon_intra_hand_bi_grams,2)
    report.ngram_data[:uncommon_intra_digram_r2p_unstressed] = kst.measure_multi_ngram_r2p(TextSession.uncommon_intra_hand_bi_grams,1) /
      kst.count_multi_ngram(TextSession.uncommon_intra_hand_bi_grams,1)
    report.ngram_data[:uncommon_intra_digram_r2p_stressed] = kst.measure_multi_ngram_r2p(TextSession.uncommon_intra_hand_bi_grams,2) /
      kst.count_multi_ngram(TextSession.uncommon_intra_hand_bi_grams,2)
    report.ngram_data[:uncommon_intra_digram_count_unstressed] = kst.count_multi_ngram(TextSession.uncommon_intra_hand_bi_grams,1)
    report.ngram_data[:uncommon_intra_digram_count_stressed] = kst.count_multi_ngram(TextSession.uncommon_intra_hand_bi_grams,2)

    # print "calculate for uncommon bi grams done - #{Time.now - time}\n"
    # time = Time.now
    kst.calculate_double_letters
    kst.calculate_common_tri_grams
    kst.calcualte_uncommon_tri_grams

    # print "calculate for tri grams done - #{Time.now - time}\n"
    # time = Time.now

    report.save
    kst.report.save
  end

  def calculate_double_letters
    @report.ngram_data[:double_letter_digram_p2r_stressed] = measure_multi_ngram_p2r(TextSession.double_letter_bi_grams,1) /
      count_multi_ngram(TextSession.double_letter_bi_grams,1)
    @report.ngram_data[:double_letter_digram_p2r_unstressed] = measure_multi_ngram_p2r(TextSession.double_letter_bi_grams,2) /
      count_multi_ngram(TextSession.double_letter_bi_grams,2)
    @report.ngram_data[:double_letter_digram_r2p_stressed] = measure_multi_ngram_p2r(TextSession.double_letter_bi_grams,1) /
      count_multi_ngram(TextSession.double_letter_bi_grams,1)
    @report.ngram_data[:double_letter_digram_r2p_unstressed] = measure_multi_ngram_p2r(TextSession.double_letter_bi_grams,1) /
      count_multi_ngram(TextSession.double_letter_bi_grams,2)
    @report.ngram_data[:double_letter_digram_count_stressed] = count_multi_ngram(TextSession.double_letter_bi_grams,1)
    @report.ngram_data[:double_letter_digram_count_unstressed] = count_multi_ngram(TextSession.double_letter_bi_grams,2)
  end

  def calculate_common_tri_grams
    @report.ngram_data[:common_tri_grams_p2r_stressed] = measure_multi_ngram_p2r(TextSession.common_tri_grams,1) /
      count_multi_ngram(TextSession.common_tri_grams,1)
    @report.ngram_data[:common_tri_grams_p2r_unstressed] = measure_multi_ngram_p2r(TextSession.common_tri_grams,2) /
      count_multi_ngram(TextSession.common_tri_grams,2)
    @report.ngram_data[:common_tri_grams_r2p_stressed] = measure_multi_ngram_r2p(TextSession.common_tri_grams,1) /
      count_multi_ngram(TextSession.common_tri_grams,1)
    @report.ngram_data[:common_tri_grams_r2p_unstressed] = measure_multi_ngram_r2p(TextSession.common_tri_grams,2) /
      count_multi_ngram(TextSession.common_tri_grams,2)
    @report.ngram_data[:common_tri_grams_count_stressed] = count_multi_ngram(TextSession.common_tri_grams,1)
    @report.ngram_data[:common_tri_grams_count_unstressed] = count_multi_ngram(TextSession.common_tri_grams,2)
  end

  def calcualte_uncommon_tri_grams
    @report.ngram_data[:uncommon_tri_grams_p2r_stressed] = measure_multi_ngram_p2r(TextSession.uncommon_tri_grams,1) /
      count_multi_ngram(TextSession.uncommon_tri_grams,1)
    @report.ngram_data[:uncommon_tri_grams_p2r_unstressed] = measure_multi_ngram_p2r(TextSession.uncommon_tri_grams,2) /
      count_multi_ngram(TextSession.uncommon_tri_grams,2)
    @report.ngram_data[:uncommon_tri_grams_r2p_stressed] = measure_multi_ngram_r2p(TextSession.uncommon_tri_grams,1) /
      count_multi_ngram(TextSession.uncommon_tri_grams,1)
    @report.ngram_data[:uncommon_tri_grams_r2p_unstressed] = measure_multi_ngram_r2p(TextSession.uncommon_tri_grams,2) /
      count_multi_ngram(TextSession.uncommon_tri_grams,2)
    @report.ngram_data[:uncommon_tri_grams_count_stressed] = count_multi_ngram(TextSession.uncommon_tri_grams,1)
    @report.ngram_data[:uncommon_tri_grams_count_unstressed] = count_multi_ngram(TextSession.uncommon_tri_grams,2)
  end

end