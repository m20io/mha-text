class CalculateLevenshteinDistance

  def self.analyse(text_session, report)
    report.levenshtein_distance_unstressed = Text::Levenshtein.distance(
      text_session.final_text_1,text_session.personal_template_1)
    report.levenshtein_distance_stressed = Text::Levenshtein.distance(
      text_session.final_text_2,text_session.personal_template_2)
    report.save
  end

end
