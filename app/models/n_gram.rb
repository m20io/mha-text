class NGram < ActiveRecord::Base
  # ngram :string
  # position :string
  # timing_data :text

  belongs_to :text_session
  has_and_belongs_to_many :key_strokes

  store :timing_data, accessors: [
    :p2r, :r2p, :p2r_1, :r2p_1
  ]

  def average_p2r
    p2r / count.to_f
  end

  def average_r2p
    r2p / count.to_f
  end

  def average_p2r_1
    p2r_1 / count.to_f
  end

  def average_r2p_1
    r2p_1 / count.to_f
  end

end