# == Schema Information
#
# Table name: reports
#
#  id                                               :integer          not null, primary key
#  created_at                                       :datetime
#  updated_at                                       :datetime
#  levenshtein_distance_stressed                    :integer
#  levenshtein_distance_unstressed                  :integer
#  length_difference_stressed                       :integer
#  length_difference_unstressed                     :integer
#  text_session_id                                  :integer
#  average_time_distance_matrix_unstressed          :text
#  average_time_distance_stressed                   :float
#  average_time_distance_unstressed                 :float
#  average_time_distance_matrix_stressed            :text
#  count_differences_without_corrections_unstressed :integer
#  count_differences_without_corrections_stressed   :integer
#  correction_count_stressed                        :integer
#  correction_count_unstressed                      :integer
#

class Report < ActiveRecord::Base
  belongs_to :text_session
  validates :text_session, presence: true

  serialize :histogram_us, Array
  serialize :histogram_s, Array

  store :key_stroke_counts, accessors: [
    :backspace_count_unstressed, :backspace_count_stressed,
    :del_count_unstressed, :del_count_stressed,
    :space_count_unstressed, :space_count_stressed,
    :arrow_count_unstressed, :arrow_count_stressed,
    :letter_e_count_unstressed, :letter_e_count_stressed,
    :letter_n_count_unstressed, :letter_n_count_stressed,
    :letter_i_count_unstressed, :letter_i_count_stressed,
    :letter_s_count_unstressed, :letter_s_count_stressed,
    :letter_r_count_unstressed, :letter_r_count_stressed,
    :letter_a_count_unstressed, :letter_a_count_stressed,

    :pauses_unstressed, :pauses_stressed,
    :pause_time_unstressed, :pause_time_stressed,

    :backspace_time_unstressed, :backspace_time_stressed,
    :del_time_unstressed, :del_time_stressed,
    :space_time_unstressed, :space_time_stressed,
    :arrow_time_unstressed, :arrow_time_stressed,
    :letter_e_time_unstressed, :letter_e_time_stressed,
    :letter_n_time_unstressed, :letter_n_time_stressed,
    :letter_i_time_unstressed, :letter_i_time_stressed,
    :letter_s_time_unstressed, :letter_s_time_stressed,
    :letter_r_time_unstressed, :letter_r_time_stressed,
    :letter_a_time_unstressed, :letter_a_time_stressed,

    :average_p2r_time_unstressed, :average_p2r_time_stressed,
    :average_r2p_time_unstressed, :average_r2p_time_stressed,

    :inter_word_count_unstressed, :inter_word_count_stressed,
    :inter_word_fly_time_unstressed, :inter_word_fly_time_stressed,

    :capital_case_starting_letter_count_unstressed, :capital_case_starting_letter_count_stressed,
    :capital_case_starting_letter_p2r_unstressed, :capital_case_starting_letter_p2r_stressed,
    :capital_case_starting_letter_r2p_unstressed, :capital_case_starting_letter_r2p_stressed,
    :lower_case_starting_letter_count_unstressed, :lower_case_starting_letter_count_stressed,
    :lower_case_starting_letter_p2r_unstressed, :lower_case_starting_letter_p2r_stressed,
    :lower_case_starting_letter_r2p_unstressed, :lower_case_starting_letter_r2p_stressed,
    :in_word_letter_count_unstressed, :in_word_letter_count_stressed,
    :in_word_letter_p2r_unstressed, :in_word_letter_p2r_stressed,
    :in_word_letter_r2p_unstressed, :in_word_letter_r2p_stressed,

    :common_letter_p2r_unstressed, :common_letter_p2r_stressed,
    :common_letter_r2p_unstressed, :common_letter_r2p_stressed,
    :uncommon_letter_p2r_unstressed, :uncommon_letter_p2r_stressed,
    :uncommon_letter_r2p_unstressed, :uncommon_letter_r2p_stressed
  ]

  store :word_timing, accessors: [
    :common_word_count_unstressed, :common_word_count_stressed,
    :common_word_p2r_unstressed, :common_word_p2r_stressed,
    :uncommon_word_count_unstressed, :uncommon_word_count_stressed,
    :uncommon_word_p2r_unstressed, :uncommon_word_p2r_stressed,
  ]

  serialize :ngram_data, Hash

  def self.build_from_text_session(text_session)
    report = Report.new
    report.text_session = text_session

    report
  end

  def group
    return "group1" if self.text_session.user_id.include?"G1"
    return "group2" if self.text_session.user_id.include?"G2"
    return "group4" if self.text_session.user_id.include?"G4"
  end


  def levenshtein_measure
    self.levenshtein_distance_unstressed - self.levenshtein_distance_stressed
  end

  def self.generate_csv(options = {})
    text_session_column_names = %w[user_id]
    report_column_names = %w[key_stroke_count_unstressed key_stroke_count_stressed
      levenshtein_distance_stressed levenshtein_distance_unstressed
      correction_count_stressed correction_count_unstressed]

    # Feature Count X
    report_key_stroke_counts_names = %w[
      backspace_count_stressed backspace_count_unstressed
      del_count_stressed del_count_unstressed
      space_count_stressed space_count_unstressed
      arrow_count_stressed arrow_count_unstressed ]

    # Feature Single Letter Average X
    report_key_stroke_counts_names += %w[
      letter_e_time_unstressed letter_e_time_stressed
      letter_n_time_unstressed letter_n_time_stressed
      letter_i_time_unstressed letter_i_time_stressed
      letter_s_time_unstressed letter_s_time_stressed
      letter_r_time_unstressed letter_r_time_stressed
      letter_a_time_unstressed letter_a_time_stressed
      backspace_time_unstressed backspace_time_stressed
      del_time_unstressed del_time_stressed
      space_time_unstressed space_time_stressed
      arrow_time_unstressed arrow_time_stressed ]

    # Feature Average P2R Time, Average R2P Time
    report_key_stroke_counts_names += %w[
      average_p2r_time_unstressed average_p2r_time_stressed
      average_r2p_time_unstressed average_r2p_time_stressed
    ]

    # Feature Inter Word Flytime
    report_key_stroke_counts_names += %w[
      inter_word_fly_time_unstressed inter_word_fly_time_stressed ]

    # Feature Common Letter Average, Uncommon Letter Average
    report_key_stroke_counts_names += %w[
      common_letter_p2r_unstressed common_letter_p2r_stressed
      common_letter_r2p_unstressed common_letter_r2p_stressed
      uncommon_letter_p2r_unstressed uncommon_letter_p2r_stressed
      uncommon_letter_r2p_unstressed uncommon_letter_r2p_stressed]

    # Feature Capital Word Starting Letter, Non-Capital Word Starting Letter
    # Non Starting Letter
    report_key_stroke_counts_names += %w[
      capital_case_starting_letter_p2r_stressed capital_case_starting_letter_p2r_unstressed
      capital_case_starting_letter_r2p_stressed capital_case_starting_letter_r2p_unstressed
      lower_case_starting_letter_p2r_stressed lower_case_starting_letter_p2r_unstressed
      lower_case_starting_letter_r2p_stressed lower_case_starting_letter_r2p_unstressed
      in_word_letter_p2r_stressed in_word_letter_p2r_unstressed
      in_word_letter_r2p_stressed in_word_letter_r2p_unstressed ]


    # Feature Common Words, Uncommon Words
    report_word_timing_names = %w[
      common_word_p2r_unstressed common_word_p2r_stressed
      common_word_r2p_unstressed common_word_r2p_stressed
      uncommon_word_p2r_unstressed uncommon_word_p2r_stressed
      uncommon_word_r2p_unstressed uncommon_word_r2p_stressed]

    report_ngrams = []

    (TextSession.common_bi_grams + TextSession.common_tri_grams).each do |ngram|
        report_ngrams << ["ngram_#{ngram}_count_unstressed", "ngram_#{ngram}_count_stressed",
        "ngram_#{ngram}_p2r_unstressed", "ngram_#{ngram}_p2r_stressed",
        "ngram_#{ngram}_r2p_unstressed", "ngram_#{ngram}_r2p_stressed" ]
    end

    report_ngrams.flatten!
    report_ngrams += %w[
      common_digram_p2r_stressed common_digram_p2r_unstressed
      common_digram_r2p_stressed common_digram_r2p_unstressed
      common_inter_digram_p2r_stressed common_inter_digram_p2r_unstressed
      common_inter_digram_r2p_stressed common_inter_digram_r2p_unstressed
      common_intra_digram_p2r_stressed common_intra_digram_p2r_unstressed
      common_intra_digram_r2p_stressed common_intra_digram_r2p_unstressed

      common_digram_p2r_1_stressed common_digram_p2r_1_unstressed
      common_inter_digram_p2r_1_stressed common_inter_digram_p2r_1_unstressed
      common_intra_digram_p2r_1_stressed common_intra_digram_p2r_1_unstressed

      uncommon_digram_p2r_stressed uncommon_digram_p2r_unstressed
      uncommon_digram_r2p_stressed uncommon_digram_r2p_unstressed
      uncommon_inter_digram_p2r_stressed uncommon_inter_digram_p2r_unstressed
      uncommon_inter_digram_r2p_stressed uncommon_inter_digram_r2p_unstressed
      uncommon_intra_digram_p2r_stressed uncommon_intra_digram_p2r_unstressed
      uncommon_intra_digram_r2p_stressed uncommon_intra_digram_r2p_unstressed

      double_letter_digram_p2r_stressed double_letter_digram_p2r_unstressed
      double_letter_digram_r2p_stressed double_letter_digram_r2p_unstressed
    ]

    report_ngrams += %w[
      common_tri_grams_p2r_stressed common_tri_grams_p2r_unstressed
      common_tri_grams_r2p_stressed common_tri_grams_r2p_unstressed
      uncommon_tri_grams_p2r_stressed uncommon_tri_grams_p2r_unstressed
      uncommon_tri_grams_r2p_stressed uncommon_tri_grams_r2p_unstressed
    ]

    CSV.generate(options) do |csv|
      csv << text_session_column_names + ["text_order"] + report_column_names +
        report_key_stroke_counts_names + report_word_timing_names + report_ngrams
      all.each do |report|
        csv << report.text_session.attributes.values_at(*text_session_column_names) +
          [report.text_session.text_order] +
          report.attributes.values_at(*report_column_names).map { |field| field.to_s.tr('.',',') } +
          report.key_stroke_counts.values_at(*report_key_stroke_counts_names.map(&:to_sym)).map { |field| field.to_s.tr('.',',') } +
          report.word_timing.values_at(*report_word_timing_names.map(&:to_sym)).map { |field| field.to_s.tr('.',',') } +
          report.ngram_data.values_at(*report.ngram_data.keys)
      end
    end
  end

end
