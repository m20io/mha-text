class ExportDifferences
  def self.generate_csv(options = {})
    text_session_column_names = %w[user_id]
    report_column_names = %w[
      key_stroke_count
      levenshtein_distance
      correction_count]

    # Feature Count X
    report_key_stroke_counts_names = %w[
      backspace_count
      del_count
      space_count
      arrow_count ]

    # Feature Single Letter Average X
    report_key_stroke_counts_names += %w[
      backspace_time
      del_time
      space_time
      arrow_time ]

    # Feature Average P2R Time, Average R2P Time
    report_key_stroke_counts_names += %w[
      average_p2r_time
      average_r2p_time ]

    # Feature Inter Word Flytime
    report_key_stroke_counts_names += %w[
      inter_word_fly_time
      inter_word_count ]

    # Feature Common Letter Average, Uncommon Letter Average
    report_key_stroke_counts_names += %w[
      common_letter_p2r
      common_letter_r2p
      uncommon_letter_p2r
      uncommon_letter_r2p ]

    # Feature Capital Word Starting Letter, Non-Capital Word Starting Letter
    # Non Starting Letter
    report_key_stroke_counts_names += %w[
      capital_case_starting_letter_p2r
      capital_case_starting_letter_r2p
      capital_case_starting_letter_count
      lower_case_starting_letter_p2r
      lower_case_starting_letter_r2p
      lower_case_starting_letter_count
      in_word_letter_p2r
      in_word_letter_r2p
      in_word_letter_count
    ]


    ('a'..'z').each do |l|
      report_key_stroke_counts_names += ["letter_#{l}_count", "letter_#{l}_p2r"]
    end


    # Feature Common Words, Uncommon Words
    report_word_timing_names = %w[
      common_word_p2r
      common_word_count
      uncommon_word_p2r
      uncommon_word_count ]

    report_ngrams = []


    (TextSession.common_bi_grams).each do |ngram|
        report_ngrams <<
       ["ngram_#{ngram}_p2r", "ngram_#{ngram}_r2p", "ngram_#{ngram}_count" ]
    end

   (TextSession.common_tri_grams).each do |ngram|
        report_ngrams <<
       ["ngram_#{ngram}_p2r", "ngram_#{ngram}_r2p", "ngram_#{ngram}_count" ]
    end
    ('a'..'z').each do |letter|
      report_ngrams << [
        "bi_gram_#{letter}_p2r_1", "bi_gram_#{letter}_r2p_1",
        "bi_gram_#{letter}_inter_hand_p2r_1", "bi_gram_#{letter}_inter_hand_r2p_1",
        "bi_gram_#{letter}_intra_hand_p2r_1", "bi_gram_#{letter}_intra_hand_r2p_1"
      ]
    end

    report_ngrams.flatten!
    report_ngrams += %w[
      common_digram_p2r
      common_digram_r2p
      common_digram_p2r_1
      common_digram_count

      common_inter_digram_p2r
      common_inter_digram_r2p
      common_inter_digram_p2r_1
      common_inter_digram_count

      common_intra_digram_p2r
      common_intra_digram_r2p
      common_intra_digram_p2r_1
      common_intra_digram_count

      uncommon_digram_p2r
      uncommon_digram_r2p
      uncommon_digram_count

      uncommon_inter_digram_p2r
      uncommon_inter_digram_r2p
      uncommon_inter_digram_count
      uncommon_intra_digram_p2r
      uncommon_intra_digram_r2p
      uncommon_intra_digram_count

      double_letter_digram_p2r
      double_letter_digram_r2p
      double_letter_digram_count
    ]

    report_ngrams += %w[
      common_tri_grams_p2r
      common_tri_grams_r2p
      common_tri_grams_count
      uncommon_tri_grams_p2r
      uncommon_tri_grams_r2p
      uncommon_tri_grams_count
    ]

    names = []
    10.times.each{|i| names << "hist_#{i}" }

    CSV.generate(options) do |csv|
      csv << text_session_column_names + ["text_order"] + report_column_names +
        report_key_stroke_counts_names + report_word_timing_names + report_ngrams +
        names
      Report.all.each do |report|
        values = []
        values += report_column_names.map do |cn|
          if report["#{cn}_unstressed"].nil? || \
             report["#{cn}_stressed"].nil?
            0
          else
            report["#{cn}_unstressed"] - report["#{cn}_stressed"]
          end
        end
        values += report_key_stroke_counts_names.map do |cn|
          if report.key_stroke_counts["#{cn}_unstressed".to_sym].nil? || \
            report.key_stroke_counts["#{cn}_stressed".to_sym].nil?
            0
          else
            report.key_stroke_counts["#{cn}_unstressed".to_sym] - report.key_stroke_counts["#{cn}_stressed".to_sym]
          end
        end

        values += report_word_timing_names.map do |cn|
          if report.word_timing["#{cn}_unstressed".to_sym].nil? || \
             report.word_timing["#{cn}_stressed".to_sym].nil?
            0
          else
            report.word_timing["#{cn}_unstressed".to_sym] - report.word_timing["#{cn}_stressed".to_sym]
          end
        end

        values += report_ngrams.map do |cn|
          if report.ngram_data["#{cn}_unstressed".to_sym].nil? || \
             report.ngram_data["#{cn}_stressed".to_sym].nil?
            0
          else
            report.ngram_data["#{cn}_unstressed".to_sym] - report.ngram_data["#{cn}_stressed".to_sym]
          end
        end

        10.times.each{|i| values << report.histogram_us[i] - report.histogram_s[i] }
        csv << report.text_session.attributes.values_at(*text_session_column_names) +
          [report.text_session.text_order] + values

      end
    end
  end
end

