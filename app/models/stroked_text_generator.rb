class StrokedTextGenerator

  def self.analyse(text_session,report = nil)
    text_session

    capslock_on = false
    (1..2).each do |position|
      text = ""
      KeyStroke.where(:text_session_id => text_session.id).where(:position => position).
      where(:event_type => "keydown").each do |key|
        raise "no letter for #{key.stroked_key}" if key.letter.nil?
        if key.letter == '#caps_lock#'
          if capslock_on
            capslock_on = false
          else
            capslock_on = true
          end
          next
        end
        if capslock_on
          key.shift_key = true
        end
        text += key.letter
      end
      text_session.stroked_text_1 = text if position == 1
      text_session.stroked_text_2 = text if position == 2
    end
    text_session.save
  end
end