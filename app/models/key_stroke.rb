# encoding: utf-8
# == Schema Information
#
# Table name: key_strokes
#
#  id              :integer          not null, primary key
#  event_type      :string(255)
#  stroked_key     :integer
#  ctrl_key        :boolean
#  alt_key         :boolean
#  shift_key       :boolean
#  target          :string(255)
#  time_stamp      :integer
#  created_at      :datetime
#  updated_at      :datetime
#  text_session_id :integer
#  position        :string(255)
#
class KeyStroke < ActiveRecord::Base
  belongs_to :text_session

  has_one :down_event, :class_name => "KeyStroke",
    :foreign_key => "up_event_id"
  belongs_to :up_event, class_name: "KeyStroke"

  has_and_belongs_to_many :n_grams

  store :timing_data, accessors: [
    :p2r, :r2p
  ]

  def letter
    letter = KeyStroke.keymap.invert[self.stroked_key.to_s]

    if letter.nil?
      letter = "\#KEY#{self.stroked_key}\#"
    end

    if self.shift_key && !letter.in?(KeyStroke.non_upcaseable_keys)
      letter = letter.upcase
    end

    letter
  end



  def measure_timing_data
    if self.event_type == 'keydown' && self.up_event.present?
      self.p2r = self.up_event.time_stamp - self.time_stamp
      next_down = self.text_session.key_strokes.where(position: self.position).
        where("time_stamp > #{self.up_event.time_stamp}").order(:time_stamp).first
      if next_down.present? && next_down.event_type == 'keydown'
        self.r2p = next_down.time_stamp - self.up_event.time_stamp
        self.r2p = KeyStroke.max_r2p if self.r2p > KeyStroke.max_r2p
      else
        self.r2p = 0
      end
      self.save
    end
  end

  def self.max_r2p
    1000
  end

  def self.special_keys
      [",",
      ".",
      "-",
      "-",
      "+",
      "?",
      "]}",
      "#tab#",
      "#left#",
      "#up#",
      "#right#",
      "#down#",
      "#end#",
      " ",
      "#alt#",
      "#strg#",
      "#shift#",
      "#caps_lock#",
      "#backspace#",
      "#enter#",
      "#delete#",
      "#insert#",
      "#right_win#",
      "#win_menu#",
      "#unknown#"]
  end

  def self.non_upcaseable_keys
    ["#shift#"]
  end

  def self.keymap
    { "a" => "65",
      "b" => "66",
      "c" => "67",
      "d" => "68",
      "e" => "69",
      "f" => "70",
      "g" => "71",
      "h" => "72",
      "i" => "73",
      "j" => "74",
      "k" => "75",
      "l" => "76",
      "m" => "77",
      "n" => "78",
      "o" => "79",
      "p" => "80",
      "q" => "81",
      "r" => "82",
      "s" => "83",
      "t" => "84",
      "u" => "85",
      "v" => "86",
      "w" => "87",
      "x" => "88",
      "y" => "89",
      "z" => "90",
      "1" => "49",
      "1num" => "97",
      "2" => "50",
      "3" => "51",
      "4" => "52",
      "5" => "53",
      "6" => "54",
      "6num" => "102",
      "7" => "55",
      "8" => "56",
      "9" => "57",
      "9num" => "105",
      "0" => "58",
      "0+" => "48",
      "0num" => "96",
      "," => "188",
      "." => "190",
      "-" => "189",
      "-" => "109",
      "+" => "107",
      "ö+" => "186",
      "ä" => "222",
      "ß" => "219",
      "ü" => "59",
      "?" => "191",
      "ö" => "192",
      "]}" => "221",
      "#tab#" => "9",
      "#left#" => "37",
      "#up#" => "38",
      "#right#" => "39",
      "#down#" => "40",
      "#end#" => "35",
      " " => "32",
      "#alt#" => "18",
      "#strg#" => "17",
      "#shift#" => "16",
      "#caps_lock#" => "20",
      "#backspace#" => "8",
      "#enter#" => "13",
      "#delete#" => "46",
      "#insert#" => "45",
      "#right_win#" => "92",
      "#win_menu#" => "93",
      "#unknown#" => "226"
    }
  end
end
