# encoding: utf-8
# == Schema Information
#
# Table name: text_sessions
#
#  id                  :integer          not null, primary key
#  user_id             :string(255)
#  text_1              :string(255)
#  text_2              :string(255)
#  created_at          :datetime
#  updated_at          :datetime
#  final_text_1        :text
#  final_text_2        :text
#  stroked_text_1      :text
#  stroked_text_2      :text
#  personal_template_1 :text
#  personal_template_2 :text
#

class TextSession < ActiveRecord::Base
  has_many :key_strokes
  has_many :n_grams

  def self.proper_filled
    where("LENGTH( final_text_1) > 200").where("LENGTH( final_text_2) > 200")
  end


  def self.text_options
    ["Text A", "Text B"]
  end

  def has_results
    true
  end

  def self.uncommon_words
    { TextSession.text_options[0] =>
      ["Silikon-Kautschuk", "Gipsmantel", "Erkaltens", "Gußkanäle", "Gipsplastik", "Luftkanäle", "Spezialofen", "entweichende", "abnehmbare", "Tonschicht", "Erkalten", "Gußform", "konisch", "Bronzeguß", "Wachsschicht", "weggeschlagen", "dünneren", "eindringende", "dickeren"],
      TextSession.text_options[1] =>
      ["Vorderteils", "aufgestickt", "Piratenlook", "Halsausschnitt", "Goldknöpfe", "Naturfaser", "Pullovers", "betonend", "Schulterklappen", "gewebte", "Schnalle", "Bündchen", "abgeknöpft", "Kimono", "modisches", "Hosenbund", "Goldkettchen", "Gelungen", "beständiger", "Stricken"]
    }
  end

  def self.common_letters
    %w[e n i s r a t d h u]
  end

  def self.left_hand_letters
    %w[q w e r t a s d f g y x c v b]
  end

  def self.right_hand_letters
    %w[z u i o p ü h j k l ö ä n m ß]
  end

  def self.common_bi_grams
    ["en", "er", "ch", "de", "ei", "ge", "in", "ne", "ie", "st"]

    # { TextSession.text_options[0] =>
    #   [["en", 43], ["er", 38], ["ch", 36], [" d", 28], ["de", 27], ["in", 24], ["ei", 21], ["ge", 18], ["ne", 16], [" w", 15], [" e", 15], ["an", 15], ["wi", 14], ["nd", 14], ["rd", 13], ["ie", 13], ["au", 13], ["te", 13], ["it", 13], ["sc", 13], [" a", 13]],
    #   TextSession.text_options[1] =>
    #   [[["en", 47], ["er", 35], [" d", 33], ["ch", 31], ["de", 27], ["ge", 23], ["ie", 22], ["st", 21], ["ei", 20], ["es", 19], ["te", 18], ["nd", 17], ["in", 17], ["el", 16], ["is", 14], ["se", 13], ["ri", 13], ["be", 13], ["zu", 12], [" S", 12], ["re", 12]]]
    # }
  end

  def self.inter_hand_bi_grams
    ["en", "ch", "ei", "ne", "ie"]
  end

  def self.intra_hand_bi_grams
    %w[er de ge in st]
  end
  def self.uncommon_inter_hand_bi_grams
    TextSession.all_bi_grams.map{|dg| dg if (TextSession.left_hand_letters.any?{|l| dg.include?(l)} && \
      TextSession.right_hand_letters.any? {|l| dg.include?(l)} )}.compact
  end

  def self.uncommon_intra_hand_bi_grams
    TextSession.all_bi_grams.map{|dg| dg if !(TextSession.left_hand_letters.any?{|l| dg.include?(l)}  && \
      TextSession.right_hand_letters.any? {|l| dg.include?(l)} )}.compact
  end

  def self.uncommon_bi_grams
    TextSession.all_bi_grams - TextSession.common_bi_grams
  end

  def self.double_letter_bi_grams
    TextSession.all_bi_grams.map{|dg| dg if dg[0] == dg[1]}.compact
  end
  def self.all_bi_grams
    ["be", "ei", "im", "br", "ro", "on", "nz", "ze", "eg", "gu", "uß", "wi", "ir",      "rd", "me", "is", "st", "vo", "in", "ne", "er", "gi", "ip", "ps", "sp", "pl",      "la", "as", "ti", "ik", "au", "us", "sg", "ge", "ga", "an", "ng", "en", "di",      "ie", "es", "se", "mi", "it", "et", "tw", "wa", "cm", "ic", "ck", "ke",      "to", "ns", "sc", "ch", "hi", "ht", "so", "ab", "bg", "ed", "de", "ec",      "kt", "da", "aß", "mo", "od", "el", "ll", "je", "id", "hä", "äl", "lf",      "ft", "te", "ol", "lk", "ko", "om", "mm", "ni", "rk", "ar", "rü", "üb",      "sm", "ma", "nt", "le", "gt", "he", "ra", "no", "un", "nd", "du", "ur",      "rc", "ka", "nä", "zw", "nr", "um", "sk", "ku", "ul", "lp", "pt", "kr",      "tu", "at", "19", "96", "60", "si", "il", "li", "ut", "ts", "hu",      "uk", "ef", "fü", "ül", "lt", "ib", "bt", "tä", "är", "na", "uf",      "fl", "ag", "al", "ls", "oh", "hn", "hw", "ri", "ig", "gk", "or",      "rs", "eh", "bn", "hm", "mb", "ba", "re", "ßf", "fo", "rm", "nb",      "ßk", "ha", "tb", "ve", "nn", "eo", "ob", "rf", "lä", "äc", "zu",      "hs", "ac", "tr", "hl", "eß", "ße", "go", "os", "ss", "gr", "ad",      "dü", "ün", "hü", "üs", "ho", "mk", "rn", "rh", "lb", "am", "ot",      "tt", "rz", "fe", "up", "pf", "if", "we", "ih", "hr", "ue", "fi",      "ix", "xi", "rt", "ür", "dr", "lu", "tk", "mü", "eb", "nu", "lz",      "em", "pe", "ez", "zi", "ia", "lo", "of", "tz", "zt", "än", "sz",      "ta", "lr", "lü", "mh", "gg", "bo", "ki", "bi", "ap", "pi", "sa", "ug", "kl",      "nf", "af", "fr", "ja", "pa", "ds", "nw", "hö", "öp", "r:", "vi", "iß", "gl", "üh",      "fa", "rb", "oc", "ew", "ff", "az", "mu", "nl", "oo", "ok", "fg", "n:", "lg", "eu",      "ee", "g!", "bü", "dc", "pu", "ov", "do", "op", "pp", "Är", "nv", "rl", "kö", "ön", "ek",      "kn", "nö", "wo", "bu", "nk", "ks", "mp", "lm", "pr", "rä", "äs", "bl", "tl", "ßt", "ld",      "dk", "gü", "dg", "d;", "sw", "tc", "ai", "üß", "aa", "fu", "uv", "gn", "su", "uc", "üc"]
  end

  def self.common_tri_grams
    ["der", "sch", "che"]
    # position 1
    # ["DER", "SCH", "INE", "EIN", "CHE", "NTE", "MIT", "EIT"]
    # position 2
    # ["DER", "UND", "SCH", "HEN", "AUS", "CHE", "BEN", "LLE"]
  end

  def self.uncommon_tri_grams
    TextSession.all_tri_grams - TextSession.common_tri_grams
  end

  def self.all_tri_grams
    ["bei", "eim", "bro", "ron", "onz", "nze", "zeg", "egu", "guß", "wir", "ird", "mei", "eis", "ist", "von", "ein", "ine", "ner", "gip", "ips", "psp", "spl", "pla", "las", "ast", "sti", "tik", "aus", "usg", "sge", "geg", "ega", "gan", "ang", "nge", "gen", "die", "ies", "ese", "wir", "ird", "mit", "ein", "ine", "ner", "etw", "twa", "dic", "ick", "cke", "ken", "ton", "ons", "nsc", "sch", "chi", "hic", "ich", "cht", "abg", "bge", "ged", "ede", "dec", "eck", "ckt", "daß", "das", "mod", "ode", "del", "ell", "jed", "ede", "der", "der", "bei", "eid", "ide", "den", "häl", "älf", "lft", "fte", "ten", "vol", "oll", "llk", "lko", "kom", "omm", "mme", "men", "kon", "oni", "nis", "isc", "sch", "wir", "irk", "rkt", "dar", "arü", "rüb", "übe", "ber", "wir", "ird", "ein", "dic", "ick", "cke", "ker", "gip", "ips", "psm", "sma", "man", "ant", "nte", "tel", "gel", "ele", "leg", "egt", "der", "ton", "wir", "ird", "her", "era", "rau", "aus", "usg", "sge", "gen", "eno", "nom", "omm", "mme", "men", "und", "dur", "urc", "rch", "kan", "anä", "näl", "äle", "gip", "ips", "psm", "sma", "man", "ant", "nte", "tel", "der", "zwi", "wis", "isc", "sch", "che", "hen", "enr", "nra", "rau", "aum", "zwi", "wis", "isc", "sch", "che", "hen", "man", "ant", "nte", "tel", "und", "sku", "kul", "ulp", "lpt", "ptu", "tur", "mit", "gel", "ela", "lat", "ati", "tin", "ine", "ode", "der", "sei", "eit", "196", "960", "mit", "sil", "ili", "lik", "iko", "kon", "kau", "aut", "uts", "tsc", "sch", "chu", "huk", "gef", "efü", "fül", "üll", "llt", "das", "gib", "ibt", "stä", "tär", "ärk", "rke", "der", "ton", "ona", "nau", "auf", "ufl", "fla", "lag", "age", "ein", "ine", "ela", "las", "ast", "sti", "tis", "isc", "sch", "che", "als", "lso", "ohn", "hne", "sch", "chw", "hwi", "wie", "ier", "eri", "rig", "igk", "gke", "kei", "eit", "ite", "ten", "von", "vor", "ors", "rst", "ste", "teh", "ehe", "hen", "end", "nde", "den", "tei", "eil", "ile", "len", "abn", "bne", "neh", "ehm", "hmb", "mba", "bar", "are", "guß", "ußf", "ßfo", "for", "orm", "dur", "urc", "rch", "den", "ein", "inb", "nba", "bau", "ein", "ine", "nen", "guß", "ußk", "ßka", "kas", "ast", "ste", "ten", "wir", "ird", "die", "hal", "alt", "ltb", "tba", "bar", "ark", "rke", "kei", "eit", "des", "gip", "ips", "psm", "sma", "man", "ant", "nte", "tel", "els", "mit", "gel", "ela", "lat", "ati", "tin", "ine", "nef", "efo", "for", "orm", "ver", "ers", "rst", "stä", "tär", "ärk", "rkt", "die", "ies", "ese", "for", "orm", "wir", "ird", "inn", "nne", "nen", "als", "lso", "auf", "der", "gel", "ela", "lat", "ati", "tin", "ine", "neo", "eob", "obe", "ber", "erf", "rfl", "flä", "läc", "äch", "che", "zun", "unä", "näc", "äch", "chs", "hst", "mit", "wac", "ach", "chs", "bes", "est", "str", "tri", "ric", "ich", "che", "hen", "und", "ans", "nsc", "sch", "chl", "hli", "lie", "ieß", "eße", "ßen", "end", "mit", "wac", "ach", "chs", "aus", "usg", "sge", "geg", "ego", "gos", "oss", "sse", "sen", "das", "nac", "ach", "gra", "rad", "des", "erk", "rka", "kal", "alt", "lte", "ten", "ens", "sic", "ich", "ein", "ine", "ner", "dün", "ünn", "nne", "ner", "ere", "ren", "ode", "der", "dic", "ick", "cke", "ker", "ere", "ren", "sch", "chi", "hic", "ich", "cht", "die", "for", "orm", "leg", "egt", "das", "übe", "ber", "ers", "rsc", "sch", "chü", "hüs", "üss", "ssi", "sig", "ige", "wac", "ach", "chs", "wir", "ird", "aus", "usg", "sge", "geg", "ego", "gos", "oss", "sse", "sen", "ans", "nsc", "sch", "chl", "hli", "lie", "ieß", "eße", "ßen", "end", "wir", "ird", "der", "der", "hoh", "ohl", "hle", "for", "orm", "rmk", "mke", "ker", "ern", "inn", "nne", "ner", "erh", "rha", "hal", "alb", "der", "wac", "ach", "chs", "hss", "ssc", "sch", "chi", "hic", "ich", "cht", "mit", "ein", "ine", "ner", "sch", "cha", "ham", "amo", "mot", "ott", "tte", "gip", "ips", "mis", "isc", "sch", "chu", "hun", "ung", "gef", "efü", "fül", "üll", "llt", "die", "sic", "ich", "nac", "ach", "kur", "urz", "rze", "zer", "zei", "eit", "ver", "erf", "rfe", "fes", "est", "sti", "tig", "igt", "mit", "kup", "upf", "pfe", "fer", "ers", "rst", "sti", "tif", "ift", "fte", "ten", "wer", "erd", "rde", "den", "man", "ant", "nte", "tel", "und", "ker", "ern", "ihr", "hre", "rer", "lag", "age", "zue", "uei", "ein", "ina", "nan", "and", "nde", "der", "fix", "ixi", "xie", "ier", "ert", "guß", "ußk", "ßka", "kan", "anä", "näl", "äle", "für", "die", "ein", "ind", "ndr", "dri", "rin", "ing", "nge", "gen", "end", "nde", "bro", "ron", "onz", "nze", "und", "luf", "uft", "ftk", "tka", "kan", "anä", "näl", "äle", "für", "die", "ent", "ntw", "twe", "wei", "eic", "ich", "che", "hen", "end", "nde", "luf", "uft", "bei", "eid", "ide", "aus", "wac", "ach", "chs", "müs", "üss", "sse", "sen", "ein", "ing", "nge", "geb", "eba", "bau", "aut", "wer", "erd", "rde", "den", "nun", "wir", "ird", "die", "for", "orm", "ein", "ine", "nem", "spe", "pez", "ezi", "zia", "ial", "alo", "lof", "ofe", "fen", "erh", "rhi", "hit", "itz", "tzt", "das", "wac", "ach", "chs", "vol", "oll", "lls", "lst", "stä", "tän", "änd", "ndi", "dig", "aus", "usz", "szu", "zus", "usc", "sch", "chm", "hme", "mel", "elz", "lze", "zen", "den", "ent", "nts", "tst", "sta", "tan", "and", "nde", "den", "ene", "nen", "hoh", "ohl", "hlr", "lra", "rau", "aum", "wir", "ird", "flü", "lüs", "üss", "ssi", "sig", "ige", "bro", "ron", "onz", "nze", "geg", "ego", "gos", "oss", "sse", "sen", "nac", "ach", "der", "ere", "ren", "erk", "rka", "kal", "alt", "lte", "ten", "kan", "ann", "die", "for", "orm", "rmh", "mhü", "hül", "üll", "lle", "weg", "egg", "gge", "ges", "esc", "sch", "chl", "hla", "lag", "age", "gen", "wer", "erd", "rde", "den", "ang", "nge", "geb", "ebo", "bot", "von", "kim", "imo", "mon", "ono", "bis", "zum", "kap", "api", "pit", "itä", "tän", "äns", "nsa", "san", "anz", "nzu", "zug", "fol", "olk", "lkl", "klo", "lor", "ore", "mit", "ein", "inf", "nfl", "flü", "lüs", "üss", "sse", "sen", "aus", "afr", "fri", "rik", "ika", "und", "jap", "apa", "pan", "vor", "ors", "rst", "ste", "tel", "ell", "llu", "lun", "ung", "der", "tre", "ren", "end", "nds", "der", "jap", "apa", "pan", "anw", "nwe", "wel", "ell", "lle", "dur", "urc", "rch", "par", "ari", "ris", "ise", "ser", "mod", "ode", "des", "esc", "sch", "chö", "höp", "öpf", "pfe", "fer", "er:", "gib", "ibt", "vie", "iel", "wei", "eiß", "neb", "ebe", "ben", "glü", "lüh", "ühe", "hen", "end", "nde", "den", "afr", "fri", "rik", "ika", "kaf", "afa", "far", "arb", "rbe", "ben", "loc", "ock", "cke", "ker", "gew", "ewe", "web", "ebt", "bte", "sto", "tof", "off", "ffe", "aus", "nat", "atu", "tur", "urf", "rfa", "fas", "ase", "ser", "und", "daz", "azu", "str", "tre", "ren", "eng", "nge", "sch", "chn", "hni", "nit", "itt", "tte", "und", "mus", "ust", "ste", "ter", "mit", "sch", "chr", "hri", "rif", "ift", "ftz", "tze", "zei", "eic", "ich", "che", "hen", "aus", "fer", "ern", "rno", "nos", "ost", "der", "jap", "apa", "pan", "anl", "nlo", "loo", "ook", "für", "ürs", "str", "tri", "ric", "ick", "cke", "ken", "auf", "ufg", "fge", "geg", "egr", "gri", "rif", "iff", "ffe", "fen", "en:", "erf", "rfo", "fol", "olg", "ein", "ine", "ner", "neu", "eue", "uen", "ide", "dee", "vie", "iel", "ell", "lle", "lei", "eic", "ich", "cht", "ein", "bes", "est", "stä", "tän", "änd", "ndi", "dig", "ige", "ger", "erf", "rfo", "fol", "olg", "lg!", "die", "mod", "ode", "del", "ell", "lle", "die", "wir", "hie", "ier", "bes", "esc", "sch", "chr", "hre", "rei", "eib", "ibe", "ben", "sin", "ind", "von", "eig", "ige", "gen", "ene", "ner", "ele", "leg", "ega", "gan", "anz", "bün", "ünd", "ndc", "dch", "che", "hen", "und", "hal", "als", "lsa", "sau", "aus", "uss", "ssc", "sch", "chn", "hni", "nit", "itt", "des", "pul", "ull", "llo", "lov", "ove", "ver", "ers", "sin", "ind", "dop", "opp", "ppe", "pel", "elt", "ges", "est", "str", "tri", "ric", "ick", "ckt", "die", "Ärm", "rme", "mel", "mas", "asc", "sch", "che", "hen", "env", "nve", "ver", "erl", "rla", "lau", "auf", "des", "vor", "ord", "rde", "der", "ert", "rte", "tei", "eil", "ils", "geh", "eha", "hal", "alt", "lte", "ten", "kön", "önn", "nne", "nen", "abg", "bge", "gek", "ekn", "knö", "nöp", "öpf", "pft", "wer", "erd", "rde", "den", "das", "wor", "ort", "übe", "ber", "dem", "bun", "und", "lin", "ink", "nks", "das", "sch", "chw", "hwa", "war", "arz", "auf", "ufg", "fge", "ges", "est", "sti", "tic", "ick", "ckt", "ist", "bed", "ede", "deu", "eut", "ute", "tet", "übr", "bri", "rig", "ige", "gen", "ens", "feu", "eue", "uer", "ode", "der", "son", "onn", "nne", "dan", "ane", "neb", "ebe", "ben", "auf", "der", "rec", "ech", "cht", "hte", "ten", "sei", "eit", "ite", "ste", "teh", "eht", "das", "wor", "ort", "fri", "rie", "ied", "ede", "den", "ang", "nge", "geb", "ebo", "bot", "vom", "kim", "imo", "mon", "ono", "bis", "zum", "kap", "api", "pit", "itä", "tän", "äns", "nsa", "san", "anz", "nzu", "zug", "oft", "ist", "bei", "fol", "olk", "lkl", "klo", "lor", "ore", "all", "lle", "les", "ein", "ine", "nem", "sti", "til", "bel", "ela", "las", "ass", "sse", "sen", "hie", "ier", "nic", "ich", "cht", "kom", "omp", "mpl", "ple", "let", "ett", "bei", "die", "ies", "ese", "sem", "mar", "ari", "rin", "ine", "nes", "est", "sti", "til", "ilm", "lmo", "mod", "ode", "del", "ell", "ist", "der", "pir", "ira", "rat", "ate", "ten", "enl", "nlo", "loo", "ook", "prä", "räs", "äse", "sen", "ent", "der", "pir", "ira", "rat", "ate", "ten", "enb", "nbl", "blu", "lus", "use", "deu", "eut", "utl", "tli", "lic", "ich", "sic", "ich", "cht", "htb", "tba", "bar", "der", "hos", "ose", "ebe", "ben", "ens", "nso", "das", "mat", "ate", "ter", "eri", "ria", "ial", "ist", "ges", "est", "str", "tre", "rei", "eif", "ift", "die", "tas", "asc", "sch", "che", "hen", "auf", "ufg", "fge", "ges", "ese", "set", "etz", "tzt", "daz", "azu", "paß", "aßt", "die", "län", "äng", "nge", "bis", "den", "wad", "ade", "den", "jac", "ack", "cke", "ket", "ett", "gol", "old", "ldk", "dkn", "knö", "nöp", "öpf", "pfe", "daz", "azu", "sch", "chu", "hul", "ult", "lte", "ter", "erk", "rkl", "kla", "lap", "app", "ppe", "pen", "die", "sti", "tie", "ief", "efe", "fel", "str", "tre", "ren", "eng", "nge", "ger", "mar", "ari", "rin", "ine", "nes", "est", "sti", "til", "der", "gür", "ürt", "rte", "tel", "ist", "sch", "chl", "hli", "lic", "ich", "cht", "die", "sch", "chn", "hna", "nal", "all", "lle", "gol", "old", "ldg", "dgl", "glä", "län", "änz", "nze", "zen", "end", "nd;", "des", "esw", "swe", "weg", "ege", "gen", "den", "enn", "nno", "noc", "och", "auf", "uff", "ffa", "fal", "all", "lle", "len", "end", "der", "gür", "ürt", "rte", "tel", "wir", "ird", "dur", "urc", "rch", "gol", "old", "ldk", "dke", "ket", "ett", "ttc", "tch", "che", "hen", "hos", "ose", "sen", "enb", "nbu", "bun", "und", "gef", "efü", "füh", "ühr", "hrt", "die", "ies", "ese", "sem", "bei", "eis", "isp", "spi", "pie", "iel", "die", "tai", "ail", "ill", "lle", "bet", "eto", "ton", "one", "nen", "end", "die", "sch", "chm", "hma", "mal", "ale", "lin", "ini", "nie", "mac", "ach", "cht", "sch", "chl", "hla", "lan", "ank", "man", "müß", "üßt", "ßte", "sch", "cho", "hon", "gen", "ena", "nau", "hin", "ins", "nse", "seh", "ehe", "hen", "fes", "est", "stz", "tzu", "zus", "ust", "ste", "tel", "ell", "lle", "len", "ein", "paa", "aar", "pfu", "fun", "und", "zuv", "uvi", "vie", "iel", "sin", "ind", "ode", "der", "nic", "ich", "cht", "ist", "ein", "mod", "odi", "dis", "isc", "sch", "che", "hes", "ere", "rei", "eig", "ign", "gni", "nis", "daß", "der", "ver", "ers", "rsu", "suc", "uch", "geg", "egl", "glü", "lüc", "ück", "ckt", "ist", "die", "ies", "ese", "bei", "eid", "ide", "den", "sti", "til", "ilr", "lri", "ric", "ich", "cht", "htu", "tun", "ung", "nge", "gen", "ver", "ere", "rei", "ein", "ini", "nig", "ige", "gen", "dam", "ami", "mit", "etw", "twa", "was", "neu", "eue", "ues", "kre", "rei", "eie", "ier", "ere", "ren", "gel", "elu", "lun", "ung", "nge", "gen"]
  end

  def self.common_words
    ["zu", "mit", "das", "von", "den", "in", "und", "die", "der", "sich"]
  end

  def self.texts
    { TextSession.text_options[0] =>
      "Beim Bronzeguß wird meist von einer Gipsplastik ausgegangen. Diese wird mit einer etwa 1 cm dicken Tonschicht so abgedeckt, daß das Modell in jeder der beiden Hälften vollkommen konisch wirkt. Darüber wird ein dicker Gipsmantel gelegt. Der Ton wird herausgenommen und durch Kanäle im Gipsmantel der Zwischenraum zwischen Mantel und Skulptur mit Gelatine oder, seit 1960, mit Silikon-Kautschuk gefüllt. Das gibt in Stärke der Tonauflage eine elastische, also ohne Schwierigkeiten von vorstehenden Teilen abnehmbare Gußform. Durch den Einbau in einen Gußkasten wird die Haltbarkeit des Gipsmantels mit Gelatineform verstärkt. Diese Form wird innen, also auf der Gelatineoberfläche zunächst mit Wachs bestrichen und anschließend mit Wachs ausgegossen, das je nach Grad des Erkaltens sich in einer dünneren oder dickeren Schicht an die Form legt. Das überschüssige Wachs wird ausgegossen, anschließend wird der der hohle Formkern innerhalb der Wachsschicht mit einer  Schamotte-Gips-Mischung gefüllt, die sich nach kurzer zeit verfestigt. Mit Kupferstiften werden Mantel und Kern in ihrer Lage zueinander fixiert. Gußkanäle für die eindringende Bronze und Luftkanäle für die entweichende Luft, beide aus Wachs, müssen eingebaut werden. Nun wird die Form in einem Spezialofen erhitzt, um das Wachs vollständig auszuschmelzen. In den entstandenen Hohlraum wird flüssige Bronze gegossen, nach deren Erkalten kann die Formhülle weggeschlagen werden.",
    TextSession.text_options[1] =>
    "Angebot von Kimono bis zum Kapitänsanzug. Folklore mit Einflüssen aus Afrika und Japan. Vorstellung der Trends, der Japanwelle, durch Pariser Modeschöpfer: Es gibt viel Weiß neben glühenden Afrikafarben, locker gewebte Stoffe aus Naturfaser und dazu strenge Schnitte und Muster mit Schriftzeichen aus Fernost. Der Japanlook fürs Stricken aufgegriffen: Erfolg einer neuen Idee, vielleicht ein beständiger Erfolg! Die Modelle, die wir hier beschreiben, sind von eigener Eleganz. Bündchen und Halsausschnitt des Pullovers sind doppelt gestrickt. Die Ärmel, im Maschenverlauf des Vorderteils gehalten, können abgeknöpft werden. Das Wort über dem Bund links, das in Schwarz aufgestickt ist, bedeutet übrigens Feuer oder Sonne, daneben, auf der rechten Seite, steht das Wort Frieden. Angebot vom Kimono bis zum Kapitänsanzug. Oft ist bei Folklore alles in einem Stil belassen. Hier nicht komplett. Bei diesem Marinestilmodell ist der Piratenlook präsent. An der Piratenbluse deutlich sichtbar, an der Hose ebenso. Das Material ist gestreift. Die Taschen aufgesetzt. Dazu paßt die Länge – bis zu den Waden. Am Jackett Goldknöpfe, dazu Schulterklappen, die Stiefel strenger Marinestil. Der Gürtel ist schlicht, die Schnalle goldglänzend; deswegen dennoch auffallend. Der Gürtel wird durch Goldkettchen am Hosenbund geführt. In diesem Beispiel die Taille betonend. Die schmale Linie macht schlank. Man müßte schon genau hinsehen, um festzustellen, ob da ein paar Pfund zuviel sind oder nicht. Es ist ein modisches Ereignis, daß der Versuch geglückt ist, diese beiden Stilrichtungen zu vereinigen, damit etwas Neues zu kreieren. Gelungen."
    }
  end

  def text_options
    TextSession::text_options
  end

  def texts
    TextSession::texts
  end

  def text_option_unstressed
    if personal_template_1.present?
      personal_template_1[0] == "B" ? "Text A" : "Text B"
    else
      "Text A"
    end
  end

  def text_option_stressed
    if personal_template_1.present?
      personal_template_1[0] == "B" ? "Text B" : "Text A"
    else
      "Text B"
    end
  end

  def text_order
    if personal_template_1.present?
      personal_template_1[0] == "B" ? "Text A => Text B" : "Text B => Text A"
    else
      "Unknown"
    end
  end

end
