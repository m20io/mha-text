class NGramBaseTimer < BaseTimer

  def initialize(text_session, report)
    super(text_session, report)
    @cache = {}
  end

  def keydown_strokes(position)
    if position == 1
      return @keydown_strokes_position_one ||= @text_session.key_strokes.where(position: 1).where(:event_type => "keydown").
      order(:time_stamp)
    end
    if position == 2
      return @keydown_strokes_position_two ||= @text_session.key_strokes.where(position: 2).where(:event_type => "keydown").
      order(:time_stamp)
    end
  end

  def keyup_strokes(position)
    if position == 1
      return @keyup_strokes_position_one ||= @text_session.key_strokes.where(position: 1).where(:event_type => "keyup").
      order(:time_stamp)
    end
    if position == 2
      return @keyup_strokes_position_two ||= @text_session.key_strokes.where(position: 2).where(:event_type => "keyup").
      order(:time_stamp)
    end
  end

  def allkey_strokes(position)
    if position == 1
      return @allkey_strokes_position_one ||= @text_session.key_strokes.where(position: 1).order(:time_stamp)
    end
    if position == 2
      return @allkey_strokes_position_two ||= @text_session.key_strokes.where(position: 2).order(:time_stamp)
    end
  end

  def count_key_stroke_sequences(key_sequence, position)
    n_gram = get_n_gram(key_sequence,position)
    return n_gram.present? ? n_gram.count.to_f : 0.0
  end

  def measure_ngram_p2r(key_sequence, position)
    n_gram = get_n_gram(key_sequence,position)
    return n_gram.present? ? n_gram.p2r : 0
  end

  def measure_multi_ngram_p2r(ngrams,position)
    sum = 0
    ngrams.each do |ngram|
      sum += measure_ngram_p2r(ngram, position)
    end

    sum
  end

  def measure_ngram_p2r_1(key_sequence,position)
    n_gram = get_n_gram(key_sequence,position)
    return n_gram.present? ? n_gram.p2r_1 : 0
  end

  def measure_multi_ngram_p2r_1(ngrams,position)
    sum = 0
    ngrams.each do |ngram|
      sum += measure_ngram_p2r_1(ngram, position)
    end

    sum
  end

  def measure_ngram_r2p(key_sequence, position)
    n_gram = get_n_gram(key_sequence,position)
    return n_gram.present? ? n_gram.r2p : 0
  end

  def measure_ngram_r2p_1(key_sequence, position)
    n_gram = get_n_gram(key_sequence,position)
    return n_gram.present? ? n_gram.r2p_1 : 0
  end

  def measure_multi_ngram_r2p(ngrams,position)
    sum = 0
    ngrams.each do |ngram|
      sum += measure_ngram_r2p(ngram, position)
    end

    sum
  end

  def measure_multi_ngram_r2p_1(ngrams,position)
    sum = 0
    ngrams.each do |ngram|
      sum += measure_ngram_r2p_1(ngram, position)
    end

    sum
  end

  def count_multi_ngram(ngrams, position)
    count = 0
    ngrams.each do |ngram|
      count += count_key_stroke_sequences(ngram, position)
    end

    count != 0 ? count : 0.00001
  end

  def get_n_gram(key_sequence,position)
    @text_session.n_grams.select{|n_gram| n_gram.n_gram == key_sequence \
      && n_gram.position.to_s == position.to_s}.first
  end

end