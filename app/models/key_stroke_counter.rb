class KeyStrokeCounter

  def self.analyse(text_session, report)

    report.key_stroke_count_unstressed = text_session.key_strokes.where(:text_session_id => text_session.id).
      where(:position => 1).where(:event_type => "keydown").count
    report.key_stroke_count_stressed = text_session.key_strokes.where(:text_session_id => text_session.id).
      where(:position => 2).where(:event_type => "keydown").count


    report.backspace_count_unstressed, report.backspace_count_stressed = KeyStrokeCounter.
      count_key_strokes(text_session,'#backspace#')
    report.del_count_unstressed, report.del_count_stressed = KeyStrokeCounter.
      count_key_strokes(text_session,'#delete#')
    report.space_count_unstressed, report.space_count_stressed = KeyStrokeCounter.
      count_key_strokes(text_session,' ')


    report.arrow_count_unstressed, report.arrow_count_stressed = KeyStrokeCounter.count_key_strokes(text_session,'#left#')

    arrow_count_unstressed, arrow_count_stressed = KeyStrokeCounter.count_key_strokes(text_session,'#right#')
    report.arrow_count_unstressed += arrow_count_unstressed
    report.arrow_count_stressed += arrow_count_stressed

    arrow_count_unstressed, arrow_count_stressed = KeyStrokeCounter.count_key_strokes(text_session,'#up#')
    report.arrow_count_unstressed += arrow_count_unstressed
    report.arrow_count_stressed += arrow_count_stressed

    arrow_count_unstressed, arrow_count_stressed = KeyStrokeCounter.count_key_strokes(text_session,'#down#')
    report.arrow_count_unstressed += arrow_count_unstressed
    report.arrow_count_stressed += arrow_count_stressed

    ("a".."z").each do |l|
      result = KeyStrokeCounter.count_key_strokes(text_session,l)
      report.key_stroke_counts["letter_#{l}_count_unstressed".to_sym] =
        result[0]
      report.key_stroke_counts["letter_#{l}_count_stressed".to_sym] =
        result[1]
    end

    report.save
  end

  def self.normalize(report)
    report.space_count_unstressed = report.space_count_unstressed / Float(report.key_stroke_count_unstressed)
    report.del_count_unstressed = report.del_count_unstressed / Float(report.key_stroke_count_unstressed)
    report.backspace_count_unstressed = report.backspace_count_unstressed / Float(report.key_stroke_count_unstressed)
    report.arrow_count_unstressed = report.arrow_count_unstressed / Float(report.key_stroke_count_unstressed)

    report.space_count_stressed = report.space_count_stressed / Float(report.key_stroke_count_stressed)
    report.del_count_stressed = report.del_count_stressed / Float(report.key_stroke_count_stressed)
    report.backspace_count_stressed = report.backspace_count_stressed / Float(report.key_stroke_count_stressed)
    report.arrow_count_stressed = report.arrow_count_stressed / Float(report.key_stroke_count_stressed)
  end

  def self.count_key_strokes(text_session,key)
    [text_session.key_strokes.where(:text_session_id => text_session.id).
      where(:position => 1).where(:event_type => "keydown").
      where(:stroked_key => KeyStroke.keymap[key]).count,
    text_session.key_strokes.where(:text_session_id => text_session.id).
      where(:position => 2).where(:event_type => "keydown").
      where(:stroked_key => KeyStroke.keymap[key]).count]
  end

end