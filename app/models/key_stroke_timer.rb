class KeyStrokeTimer < BaseTimer
    # :pauses_unstressed, :pauses_stressed,
    # :pause_time_unstressed, :pause_time_stressed,
    # :backspace_time_unstressed, :backspace_time_stressed,
    # :del_time_unstressed, :del_time_stressed,
    # :space_time_unstressed, :space_time_stressed,
    # :arrow_time_unstressed, :arrow_time_stressed,
    # :letter_e_time_unstressed, :letter_e_time_stressed,
    # :letter_n_time_unstressed, :letter_n_time_stressed,
    # :letter_i_time_unstressed, :letter_i_time_stressed,
    # :letter_s_time_unstressed, :letter_s_time_stressed,
    # :letter_r_time_unstressed, :letter_r_time_stressed,
    # :letter_a_time_unstressed, :letter_a_time_stressed

  def initialize(text_session, report)
    super(text_session, report)
    @key_strokes_1 = @text_session.key_strokes.where(position:1)
    @key_strokes_2 = @text_session.key_strokes.where(position:2)
  end

  def self.analyse(text_session, report)
    time_start = Time.now
    key_stroke_timer = KeyStrokeTimer.new(text_session, report)

    report.backspace_time_unstressed, report.backspace_time_stressed =
      key_stroke_timer.calculate_average_key_p2r('#backspace#')

    report.del_time_unstressed, report.del_time_stressed =
      key_stroke_timer.calculate_average_key_p2r('#delete#')

    report.space_time_unstressed, report.space_time_stressed =
      key_stroke_timer.calculate_average_key_p2r(' ')

    ("a".."z").each do |l|
      result = key_stroke_timer.calculate_average_key_p2r(l)
      report.key_stroke_counts["letter_#{l}_p2r_unstressed".to_sym] =
        result[0]
      report.key_stroke_counts["letter_#{l}_p2r_stressed".to_sym] =
        result[1]
    end

    report.arrow_time_stressed, report.arrow_time_unstressed =
      key_stroke_timer.calculate_average_key_p2r('#left#')

    arrow_time_stressed, arrow_time_unstressed =
      key_stroke_timer.calculate_average_key_p2r('#right#')
    report.arrow_time_stressed += arrow_time_stressed
    report.arrow_time_unstressed += arrow_time_unstressed

    arrow_time_stressed, arrow_time_unstressed =
      key_stroke_timer.calculate_average_key_p2r('#up#')
    report.arrow_time_stressed += arrow_time_stressed
    report.arrow_time_unstressed += arrow_time_unstressed

    arrow_time_stressed, arrow_time_unstressed =
      key_stroke_timer.calculate_average_key_p2r('#down#')
    report.arrow_time_stressed += arrow_time_stressed
    report.arrow_time_unstressed += arrow_time_unstressed

    report.average_p2r_time_unstressed = key_stroke_timer.average_p2r_time(1)
    report.average_p2r_time_stressed = key_stroke_timer.average_p2r_time(2)

    report.average_r2p_time_unstressed = key_stroke_timer.average_r2p_time(1)
    report.average_r2p_time_stressed = key_stroke_timer.average_r2p_time(2)

    report.common_letter_p2r_unstressed = key_stroke_timer.measure_key_group_p2r(TextSession.common_letters,"1")
    report.common_letter_p2r_stressed = key_stroke_timer.measure_key_group_p2r(TextSession.common_letters,"2")
    report.common_letter_r2p_unstressed = key_stroke_timer.measure_key_group_r2p(TextSession.common_letters,"1")
    report.common_letter_r2p_stressed = key_stroke_timer.measure_key_group_r2p(TextSession.common_letters,"2")

    report.uncommon_letter_p2r_unstressed = key_stroke_timer.calculate_except_key_group_p2r(TextSession.common_letters, "1")
    report.uncommon_letter_p2r_stressed = key_stroke_timer.calculate_except_key_group_p2r(TextSession.common_letters, "2")
    report.uncommon_letter_r2p_unstressed = key_stroke_timer.calculate_except_key_group_r2p(TextSession.common_letters, "1")
    report.uncommon_letter_r2p_stressed = key_stroke_timer.calculate_except_key_group_r2p(TextSession.common_letters, "2")

    report.save

    # print "Analyse ran for #{Time.now - time_start} sec.\n"
  end

  def measure_key_group_p2r(key_group, position)
    key_strokes = find_key_stroke_for_key(key_group,position).
      select{|ks|ks.event_type == 'keydown'}.sort_by(&:time_stamp)
    measure_key_stroke_p2r_time(key_strokes, position) / key_strokes.count.to_f
  end

  def measure_key_group_r2p(key_group, position)
    key_strokes = find_key_stroke_for_key(key_group,position).
      select{|ks|ks.event_type == 'keydown'}.sort_by(&:time_stamp)
    measure_key_stroke_r2p_time(key_strokes, position) / key_strokes.count.to_f
  end

  def calculate_except_key_group_p2r(key_group, position)
    key_group += KeyStroke.special_keys
    key_group += [" "]
    key_codes = KeyStroke.keymap.values - key_group.map{|key| KeyStroke.keymap[key]}

    key_strokes = find_key_stroke_for_key(key_group,position).
      select{|ks|ks.event_type == 'keydown'}.sort_by(&:time_stamp)
    measure_key_stroke_p2r_time(key_strokes, position) / key_strokes.count.to_f
  end

  def calculate_except_key_group_r2p(key_group, position)
    key_group += KeyStroke.special_keys
    key_group += [" "]
    key_codes = KeyStroke.keymap.values - key_group.map{|key| KeyStroke.keymap[key]}

    key_strokes = find_key_stroke_for_key(key_group,position).
      select{|ks|ks.event_type == 'keydown'}.sort_by(&:time_stamp)
    measure_key_stroke_r2p_time(key_strokes, position) / key_strokes.count.to_f
  end

  def calculate_average_key_p2r(key)
    result = []
    key_strokes_to_be_measured = find_key_stroke_for_key(key,1)
    result[0] = measure_key_stroke_p2r_time(key_strokes_to_be_measured, 1) / count_letter(key,1)
    key_strokes_to_be_measured = find_key_stroke_for_key(key,2)
    result[1] = measure_key_stroke_p2r_time(key_strokes_to_be_measured, 2) / count_letter(key,2)

    result
  end

  def average_p2r_time(position)
    key_strokes_to_be_measured = @text_session.key_strokes.where(position: position).where(event_type: "keydown")
    measure_key_stroke_p2r_time(key_strokes_to_be_measured, 1) / key_strokes_to_be_measured.count.to_f
  end

  def average_r2p_time(position)
    key_strokes_to_be_measured = @text_session.key_strokes.where(position: position).where(event_type: "keydown")
    measure_key_stroke_r2p_time(key_strokes_to_be_measured, 1) / key_strokes_to_be_measured.count.to_f
  end

  def find_key_stroke_for_key(key_group,position)
    key_group = [key_group] unless key_group.is_a?(Array)

    if position.to_i == 1
      @key_strokes_1.where(stroked_key: key_group.map{|key| KeyStroke.keymap[key].to_i})
    else
      @key_strokes_2.where(stroked_key: key_group.map{|key| KeyStroke.keymap[key].to_i})
    end
  end

end