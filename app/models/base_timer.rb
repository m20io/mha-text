class BaseTimer

  def initialize(text_session, report)
    @text_session = text_session
    @report = report
  end

  def text_session
    @text_session
  end

  def report
    @report
  end

  def count_letter(letter, position)
    count = @text_session.key_strokes.select{|ks|ks.position.to_i == position.to_i && \
      ks.event_type == 'keydown' && ks.letter == letter}.count.to_f
    count += 0.00001 if count == 0

    return count
  end

  def measure_key_stroke_p2r_time(key_strokes_to_be_measured,position)
    return key_strokes_to_be_measured.compact.map(&:p2r).compact.sum

    position = position.to_s
    key_stroke_temp = @text_session.key_strokes.select{|ks| ks.position == position}.sort_by(&:time_stamp)
    time = 0
    key_strokes_to_be_measured.each do |ks|
      key_stroke_temp.select!{|ev| ev.time_stamp > ks.time_stamp}

      next if ks.up_event.blank?
      time += ks.up_event.time_stamp - ks.time_stamp
    end

    time
  end

  def measure_key_stroke_r2p_time(key_strokes_to_be_measured,position)
    return key_strokes_to_be_measured.compact.map(&:r2p).compact.sum

    position = position.to_s
    key_stroke_temp = @text_session.key_strokes.select{|ks| ks.position == position}.sort_by(&:time_stamp)

    time = 0
    key_strokes_to_be_measured.each do |ks|
      key_stroke_temp.select!{|ev| ev.time_stamp > ks.time_stamp}

      next if ks.up_event.blank? || key_stroke_temp.first.blank?
      if ks.up_event.time_stamp <= key_stroke_temp.first.time_stamp #is up next or is an other key allready pressed?
        next_event = key_stroke_temp.select{|ev| ev.event_type == "keydown"}.first
        time += next_event.time_stamp -  ks.up_event.time_stamp if next_event.present?
      else
        time += 0
      end
    end
    time
  end
end