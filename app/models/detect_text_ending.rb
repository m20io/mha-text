require 'singleton'

class DetectTextEnding
  include Singleton

  def self.analyse(text_session, report)
    report.text_ending_index_unstressed = DetectTextEnding.instance.
      find_end_index(text_session.personal_template_1,text_session.final_text_1)
    report.text_ending_index_stressed = DetectTextEnding.instance.
      find_end_index(text_session.personal_template_2,text_session.final_text_2)
    report.save
  end

  def find_end_index(original, text)
    if original.rindex('###END###')
      return original.rindex('###END###')
    end

    original.length
  end
end