class CorrectionCounter

  def self.analyse(text_session, report)
    cc = CorrectionCounter.new
    report.correction_count_unstressed = cc.count_correction(text_session.stroked_text_1)
    report.correction_count_stressed = cc.count_correction(text_session.stroked_text_2)

    report.save
  end

  def self.correction_regexp
    [/(#backspace#|#BACKSPACE#)/, /(#delete#|#DELETE#)/,
     /(#LEFT#|#RIGHT#|#UP#|#DOWN#)/]
  end

  def count_correction(text)
    count = 0
    CorrectionCounter.correction_regexp.each do |regexp|
      count += text.scan(Regexp.new(regexp)).size
    end

    count
  end

end