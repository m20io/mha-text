class WordTimer < BaseTimer

  def initialize(text_session, report)
    super(text_session, report)
  end

  def self.analyse(text_session, report)
    word_timer = WordTimer.new(text_session, report)

    results = word_timer.calculate_uncommon_average_timing
    report.uncommon_word_count_unstressed = results[1]
    report.uncommon_word_count_stressed = results[3]
    report.uncommon_word_p2r_unstressed = results[0]
    report.uncommon_word_p2r_stressed = results[2]


    results = word_timer.calculate_common_average_timing
    report.common_word_count_unstressed = results[1]
    report.common_word_count_stressed = results[3]
    report.common_word_p2r_unstressed = results[0]
    report.common_word_p2r_stressed = results[2]

    report.save
  end

  def calculate_uncommon_average_timing
    sum_unstressed = 0
    sum_stressed = 0
    counter_us = 0
    counter_s = 0

    TextSession.uncommon_words[@text_session.text_option_unstressed].each do |word|
      find_key_strokes(word, 1, 100,1).each do |key_strokes|
        sum_unstressed += (key_strokes.last.time_stamp - key_strokes.first.time_stamp) / word.size.to_f
        counter_us += 1
      end
    end

    TextSession.uncommon_words[@text_session.text_option_stressed].each do |word|
      find_key_strokes(word, 2, 100,1).each do |key_strokes|
        sum_stressed += (key_strokes.last.time_stamp - key_strokes.first.time_stamp) / word.size.to_f
        counter_s += 1
      end
    end

    sum_unstressed = sum_unstressed / counter_us.to_f
    sum_stressed = sum_stressed / counter_s.to_f

    [sum_unstressed, counter_us, sum_stressed, counter_s]
  end

  def calculate_common_average_timing
    sum_unstressed = 0
    sum_stressed = 0
    counter_us = 0
    counter_s = 0

    TextSession.common_words.each do |word|
      find_key_strokes(word, 1, 100).each do |key_strokes|
        sum_unstressed += (key_strokes.last.time_stamp - key_strokes.first.time_stamp) / word.size.to_f
        counter_us += 1
      end

      find_key_strokes(word, 2, 100).each do |key_strokes|
        sum_stressed += (key_strokes.last.time_stamp - key_strokes.first.time_stamp) / word.size.to_f
        counter_s += 1
      end
    end

    sum_unstressed = sum_unstressed / Float(counter_us)
    sum_stressed = sum_stressed / Float(counter_s)

    [sum_unstressed, counter_us, sum_stressed, counter_s]
  end

  def key_strokes_complete_position_1
    @key_strokes_complete_position1 ||= @text_session.key_strokes.select{ |ks| ks.position == "1"}.sort_by(&:time_stamp)
  end

  def key_strokes_complete_position_2
    @key_strokes_complete_position2 ||= @text_session.key_strokes.select{ |ks| ks.position == "2"}.sort_by(&:time_stamp)
  end

  def find_key_strokes(word, position, n, toleranze = 0)
    word.downcase!
    if position.to_i == 1
      key_strokes_complete = key_strokes_complete_position_1
    else
      key_strokes_complete = key_strokes_complete_position_2
    end

    key_strokes = key_strokes_complete.select{|ks| ks.event_type == "keydown"}
    key_strokes_indexed = []

    key_strokes.each_with_index do |ks,index|
      next if ks.letter.include?('#')
      key_strokes_indexed << [ks.letter, index]
    end

    matches = []

    (0..key_strokes_indexed.size-word.size).each do |i|
      distance = Text::Levenshtein.distance(
        key_strokes_indexed[i..i+word.size-1].map{|e| e[0]}.join.downcase,
        word)
      if distance <= toleranze
        matches << i
      end
    end

    results = []
    return results if matches.empty?


    n.times do
      break if matches.blank?
      i = matches.shift
      results << key_strokes[i+1..i+word.size].map{|ks| [ks,find_up_event(ks,key_strokes_complete)]}.flatten
    end

    results
  end

  def find_up_event(key_stroke_down, key_strokes)
    key_strokes.select{|ks| ks.time_stamp > key_stroke_down.time_stamp \
      && ks.stroked_key == key_stroke_down.stroked_key}.sort_by(&:time_stamp).first

  end

end