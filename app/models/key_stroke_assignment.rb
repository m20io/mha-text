class KeyStrokeAssignment

  def self.analyse(text_session, report)
    ksa = KeyStrokeAssignment.new
    ksa.assign_key_strokes(text_session.key_strokes.where(position: 1))
    ksa.assign_key_strokes(text_session.key_strokes.where(position: 2))

    text_session.key_strokes.map(&:measure_timing_data)
  end

  def assign_key_strokes(key_strokes)
    key_strokes.sort_by(&:time_stamp)
    up_events = key_strokes.where(event_type: "keyup")
    key_strokes.where(event_type: "keydown").each do |ks|
      ks.up_event = up_events.select{|ev| ks.time_stamp < ev.time_stamp }
        .select{|ev| ks.time_stamp + 5000 > ev.time_stamp }
        .select{|ev| ev.stroked_key == ks.stroked_key}.first

      ks.save
    end
  end

end