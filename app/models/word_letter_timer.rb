class WordLetterTimer < BaseTimer

  def initialize(text_session, report)
    super(text_session, report)
     @key_stroke_subsets = {}
  end

  def self.analyse(text_session, report)
    wlt = WordLetterTimer.new(text_session,report)

    report.capital_case_starting_letter_count_unstressed = wlt.count_capital_starting_letter("1")
    report.capital_case_starting_letter_count_stressed = wlt.count_capital_starting_letter("2")

    report.capital_case_starting_letter_p2r_unstressed = wlt.measure_capital_starting_letter_p2r("1") / wlt.count_capital_starting_letter("1")
    report.capital_case_starting_letter_p2r_stressed = wlt.measure_capital_starting_letter_p2r("2") / wlt.count_capital_starting_letter("2")

    report.capital_case_starting_letter_r2p_unstressed = wlt.measure_capital_starting_letter_r2p("1") / wlt.count_capital_starting_letter("1")
    report.capital_case_starting_letter_r2p_stressed = wlt.measure_capital_starting_letter_r2p("2") / wlt.count_capital_starting_letter("2")

    report.lower_case_starting_letter_count_unstressed = wlt.count_lower_case_starting_letter("1")
    report.lower_case_starting_letter_count_stressed = wlt.count_lower_case_starting_letter("2")

    report.lower_case_starting_letter_p2r_unstressed = wlt.measure_lower_case_starting_letter_p2r("1") / wlt.count_lower_case_starting_letter("1")
    report.lower_case_starting_letter_p2r_stressed = wlt.measure_lower_case_starting_letter_p2r("2") / wlt.count_lower_case_starting_letter("2")

    report.lower_case_starting_letter_r2p_unstressed = wlt.measure_lower_case_starting_letter_r2p("1") / wlt.count_lower_case_starting_letter("1")
    report.lower_case_starting_letter_r2p_stressed = wlt.measure_lower_case_starting_letter_r2p("2") / wlt.count_lower_case_starting_letter("2")

    report.in_word_letter_count_unstressed = wlt.count_lower_case_in_word_letter("1")
    report.in_word_letter_count_stressed = wlt.count_lower_case_in_word_letter("2")

    report.in_word_letter_p2r_unstressed = wlt.measure_in_word_letter_p2r("1") / wlt.count_lower_case_in_word_letter("1")
    report.in_word_letter_p2r_stressed = wlt.measure_in_word_letter_p2r("2") / wlt.count_lower_case_in_word_letter("2")

    report.in_word_letter_r2p_unstressed = wlt.measure_in_word_letter_r2p("1") / wlt.count_lower_case_in_word_letter("1")
    report.in_word_letter_r2p_stressed = wlt.measure_in_word_letter_r2p("2") / wlt.count_lower_case_in_word_letter("2")

    report.inter_word_count_unstressed = wlt.count_spaces("1")
    report.inter_word_count_stressed = wlt.count_spaces("2")

    report.inter_word_fly_time_unstressed = wlt.measure_inter_word_r2p("1") / wlt.count_spaces("1")
    report.inter_word_fly_time_stressed = wlt.measure_inter_word_r2p("2") / wlt.count_spaces("2")

    report.save
  end

  def count_capital_starting_letter(position)
    if @key_stroke_subsets["ccs_#{position}"].present?
      return @key_stroke_subsets["ccs_#{position}"]
    end
    @key_stroke_subsets["ccs_#{position}"] = find_capital_starting_letter_key_strokes(position).count + 0.00001
  end

  def count_lower_case_starting_letter(position)
    if @key_stroke_subsets["clc_#{position}"].present?
      return @key_stroke_subsets["clc_#{position}"]
    end
    @key_stroke_subsets["clc_#{position}"] = find_lower_case_starting_letter_key_strokes(position).count + 0.00001
  end

  def count_lower_case_in_word_letter(position)
    if @key_stroke_subsets["iw_#{position}"].present?
      return @key_stroke_subsets["iw_#{position}"]
    end
    @key_stroke_subsets["iw_#{position}"] = find_in_word_letter_key_strokes(position).count + 0.00001
  end

  def count_spaces(position)
    if @key_stroke_subsets["space_#{position}"].present?
      return @key_stroke_subsets["space_#{position}"]
    end
    @key_stroke_subsets["space_#{position}"] = find_inter_word_key_strokes(position).count + 0.00001
  end

  def measure_capital_starting_letter_p2r(position)
    key_strokes_to_be_measured = find_capital_starting_letter_key_strokes(position)
    measure_key_stroke_p2r_time(key_strokes_to_be_measured,position)
  end

  def measure_capital_starting_letter_r2p(position)
    key_strokes_to_be_measured = find_capital_starting_letter_key_strokes(position)
    measure_key_stroke_r2p_time(key_strokes_to_be_measured,position)
  end

  def measure_lower_case_starting_letter_p2r(position)
    key_strokes_to_be_measured = find_lower_case_starting_letter_key_strokes(position)
    measure_key_stroke_p2r_time(key_strokes_to_be_measured,position)
  end

  def measure_lower_case_starting_letter_r2p(position)
    key_strokes_to_be_measured = find_lower_case_starting_letter_key_strokes(position)
    measure_key_stroke_r2p_time(key_strokes_to_be_measured,position)
  end

  def measure_in_word_letter_p2r(position)
    key_strokes_to_be_measured = find_in_word_letter_key_strokes(position)
    measure_key_stroke_p2r_time(key_strokes_to_be_measured,position)
  end

  def measure_in_word_letter_r2p(position)
    key_strokes_to_be_measured = find_in_word_letter_key_strokes(position)
    measure_key_stroke_r2p_time(key_strokes_to_be_measured,position)
  end

  def measure_inter_word_r2p(position)
    key_strokes_to_be_measured = find_inter_word_key_strokes(position)
    measure_key_stroke_r2p_time(key_strokes_to_be_measured,position)
  end

  # this could private... but isn't for nicer specs
  def find_capital_starting_letter_key_strokes(position)
    key_strokes = @text_session.key_strokes.where(:position => position).
                  where(event_type: 'keydown').order(:time_stamp)

    capital_starting_key_strokes = []

    count = 0
    caps_lock_down = false
    key_strokes.each_with_index do |ks, index|
      if ks.letter == '#caps_lock#'
        caps_lock_down = caps_lock_down ? false : true
      end

      is_capital_starting_letter = true
      is_capital_starting_letter &&= ks.shift_key || caps_lock_down
      is_capital_starting_letter &&= (not KeyStroke.special_keys.include?(ks.letter))
      is_capital_starting_letter &&= (key_strokes[index-1].letter == '#shift#' || index == 0)

      capital_starting_key_strokes.append(ks) if is_capital_starting_letter
    end
    capital_starting_key_strokes
  end

  def find_lower_case_starting_letter_key_strokes(position)
    key_strokes = @text_session.key_strokes.where(:position => position).
                  where(event_type: 'keydown').order(:time_stamp)
    lower_starting_key_strokes = []
    caps_lock_down = false
    key_strokes.each_with_index do |ks, index|
      if ks.letter == '#caps_lock#'
        caps_lock_down = caps_lock_down ? false : true
      end

      is_lower_case_starting_letter = true
      is_lower_case_starting_letter &&= (not (ks.shift_key || caps_lock_down))
      is_lower_case_starting_letter &&= (not KeyStroke.special_keys.include?(ks.letter))
      is_lower_case_starting_letter &&= (key_strokes[index-1].letter == ' ' || index == 0)

      lower_starting_key_strokes.append(ks) if is_lower_case_starting_letter
    end

    lower_starting_key_strokes
  end

  def find_in_word_letter_key_strokes(position)
    key_strokes = @text_session.key_strokes.where(:position => position).
                  where(event_type: 'keydown').order(:time_stamp)
    in_word_key_strokes = []
    caps_lock_down = false
    key_strokes.each_with_index do |ks, index|
      if ks.letter == '#caps_lock#'
        caps_lock_down = caps_lock_down ? false : true
      end

      is_lower_case_in_word_letter = true
      is_lower_case_in_word_letter &&= (not (ks.shift_key || caps_lock_down))
      is_lower_case_in_word_letter &&= (not KeyStroke.special_keys.include?(ks.letter))
      is_lower_case_in_word_letter &&= key_strokes[index-1].letter != ' '
      is_lower_case_in_word_letter &&= index != 0

      in_word_key_strokes.append(ks) if is_lower_case_in_word_letter
    end
    in_word_key_strokes
  end

  def find_inter_word_key_strokes(position)
    key_strokes = @text_session.key_strokes.where(:position => position).
                  where(event_type: 'keydown').order(:time_stamp)
    in_word_key_strokes = []
    key_strokes.each_with_index do |ks, index|
      is_inter_word_letter = true
      is_inter_word_letter &&= ks.letter == " "

      in_word_key_strokes.append(ks) if is_inter_word_letter
    end
    in_word_key_strokes
  end
end