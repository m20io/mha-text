class KeyStrokeHistogram < BaseTimer

  def initialize(text_session, report)
    super(text_session, report)
  end

  def self.analyse(text_session,report)
    ksh = KeyStrokeHistogram.new(text_session, report)
    report.histogram_us = ksh.create_histogram(1)
    report.histogram_s = ksh.create_histogram(2)

    report.save
  end

  def create_histogram(position)
    key_strokes = @text_session.key_strokes.where(position: position.to_s,
      event_type: 'keydown')

    histogram = Array.new(10,0)
    time = key_strokes.first.time_stamp
    count = 0
    key_strokes.each do |ks|
      if ks.time_stamp - 1000 > time || ks == key_strokes.last
        # binding.pry
        count = 9 if count >= 9
        histogram[count] += 1
        count = 1
        time = ks.time_stamp
      else
        count +=1
      end
    end
    histogram
  end
end