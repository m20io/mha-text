class NGramGenerator

  def initialize(text_session, report)
    @text_session = text_session
    @regular_key_code = (KeyStroke.keymap.keys - KeyStroke.special_keys).
      map{|key| KeyStroke.keymap[key]}.map(&:to_i)
  end

  def self.analyse(text_session, report)
    ngg = NGramGenerator.new(text_session, report)
    ngg.create_all_bi_grams(1)
    ngg.create_all_bi_grams(2)
    ngg.create_all_tri_grams(1)
    ngg.create_all_tri_grams(2)
  end

  def create_all_n_grams(position)
    create_all_bi_grams(position)
    create_all_tri_grams(position)
  end

  def create_all_bi_grams(position)
    create_all_n_grams_of_length(position,2)
  end

  def create_all_tri_grams(position)
    create_all_n_grams_of_length(position,3)
  end

  def create_all_n_grams_of_length(position,n)
    key_downs = @text_session.key_strokes.where(position: position, event_type: 'keydown')
    n_grams = {}

    key_downs.each_with_index do |ks,index|
      next if index < (n - 1)

      n_key_strokes = []
      n.times{|i| n_key_strokes << key_downs[index-i] }
      n_key_strokes.reverse!

      next if n_key_strokes.any?{|ks| ks.shift_key ||  KeyStroke.special_keys.include?(ks.letter) }

      n_gram = n_key_strokes.map(&:letter).join
      if n_grams[n_gram].blank?
        n_grams[n_gram] = NGram.new
        n_grams[n_gram].position = position
        n_grams[n_gram].text_session = @text_session
        n_grams[n_gram].n_gram = n_gram
        n_key_strokes.each{|ks| n_grams[n_gram].key_strokes << ks }
        n_grams[n_gram].p2r = 0
        n_grams[n_gram].r2p = 0
        n_grams[n_gram].p2r_1 = 0
        n_grams[n_gram].r2p_1 = 0
        n_grams[n_gram].count = 1
      else
        n_key_strokes.each{|ks| n_grams[n_gram].key_strokes << ks }
        n_grams[n_gram].count += 1
      end

      first_event = n_key_strokes[0]
      second_event = n_key_strokes[1]

      ## check p2r
      next if ks.up_event.blank?
      ## check r2p & check p2r_1 & check r2p_1
      next if first_event.up_event.blank?
      ## check r2p_1
      next if second_event.blank?

      ## add
      n_grams[n_gram].p2r += ks.up_event.time_stamp - first_event.time_stamp
      n_grams[n_gram].r2p += ks.time_stamp - first_event.up_event.time_stamp
      n_grams[n_gram].p2r_1 += first_event.up_event.time_stamp - first_event.time_stamp
      diff = second_event.time_stamp - first_event.up_event.time_stamp
      n_grams[n_gram].r2p_1 += diff > 0 ? diff : 0
    end
    n_grams.values.map(&:save)
    n_grams
  end
end