class AddEndingIndexToReports < ActiveRecord::Migration
  def change
    add_column :reports, :text_ending_index_stressed, :integer
    add_column :reports, :text_ending_index_unstressed, :integer
  end
end
