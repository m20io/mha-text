class CreateKeyStrokes < ActiveRecord::Migration
  def change
    create_table :key_strokes do |t|
      t.references :user_id
      t.string :type
      t.integer :stroked_key
      t.boolean :ctrl_key
      t.boolean :alt_key
      t.boolean :shift_key
      t.string :target
      t.decimal :time_stamp, :precision => 16

      t.timestamps
    end
  end
end
