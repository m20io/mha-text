class AddUpEventToKeyStroke < ActiveRecord::Migration
  def change
    add_column :key_strokes, :up_event_id, :integer
  end
end
