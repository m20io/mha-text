class AddHistrogramToReport < ActiveRecord::Migration
  def change
    add_column :reports, :histogram_us, :text
    add_column :reports, :histogram_s, :text
  end
end
