class ChangeNgramTable < ActiveRecord::Migration
  def change
    rename_column :n_grams, :ngram, :n_gram
  end
end
