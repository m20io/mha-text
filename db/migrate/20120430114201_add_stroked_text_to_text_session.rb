class AddStrokedTextToTextSession < ActiveRecord::Migration
  def up
    add_column :text_sessions, :stroked_text_1, :text 
    add_column :text_sessions, :stroked_text_2, :text 
  end

  def down
    remove_column :text_sessions, :stroked_text_1
    remove_column :text_sessions, :stroked_text_2
  end
end
