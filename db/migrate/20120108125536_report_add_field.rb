class ReportAddField < ActiveRecord::Migration
  def up
    add_column :reports, :levenshtein_distance_stressed, :integer
    add_column :reports, :levenshtein_distance_unstressed, :integer
    add_column :reports, :length_difference_stressed, :integer
    add_column :reports, :length_difference_unstressed, :integer
  end

  def down
    drop_column :reports, :levenshtein_distance_stressed
    drop_column :reports, :levenshtein_distance_unstressed
    drop_column :reports, :length_differnce_stressed
    drop_column :reports, :length_differnce_unstressed
  end
end
