class RenameUserIdColonmOfText < ActiveRecord::Migration
  def up
    rename_column :texts, :user_id_id, :user_id
  end

  def down
    rename_column :texts, :user_id, :user_id_id
  end
end
