class AddFinalTextsToTextSessions < ActiveRecord::Migration
  def change
    add_column :text_sessions, :final_text_1, :text
    add_column :text_sessions, :final_text_2, :text
  end
end
