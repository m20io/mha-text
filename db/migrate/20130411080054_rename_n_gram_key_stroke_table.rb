class RenameNGramKeyStrokeTable < ActiveRecord::Migration
  def change
    rename_table :n_grams_key_strokes, :key_strokes_n_grams
  end
end
