class CreateNgram < ActiveRecord::Migration
  def change
    create_table :n_grams do |t|
      t.references :text_session
      t.string :ngram
      t.string :position
      t.text   :timging_data
      t.timestamps
    end

    create_table :n_grams_key_strokes, :id => false do |t|
      t.references :n_gram
      t.references :key_stroke
    end
  end

end