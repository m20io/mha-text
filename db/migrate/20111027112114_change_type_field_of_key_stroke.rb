class ChangeTypeFieldOfKeyStroke < ActiveRecord::Migration
  def up
    rename_column :key_strokes, :type, :event_type 
  end

  def down
    rename_column :key_strokes, :type_event, :type
  end
end
