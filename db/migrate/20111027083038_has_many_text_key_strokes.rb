class HasManyTextKeyStrokes < ActiveRecord::Migration
  def change
    remove_column :key_strokes, :user_id
    add_column :key_strokes, :text_session_id, :integer
    add_column :key_strokes, :position, :string
  end
end
