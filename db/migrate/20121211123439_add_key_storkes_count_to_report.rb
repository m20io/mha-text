class AddKeyStorkesCountToReport < ActiveRecord::Migration
  def change
    add_column :reports, :key_stroke_counts, :text
  end
end
