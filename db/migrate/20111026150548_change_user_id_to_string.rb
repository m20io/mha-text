class ChangeUserIdToString < ActiveRecord::Migration
  def up
    change_column :texts,:user_id,:string
    change_column :key_strokes,:user_id,:string
  end

  def down
    change_column :texts,:user_id,:integer
    change_column :key_strokes,:user_id,:integer
  end
end
