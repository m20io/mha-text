class RenameAndAddAverageTimeDistanceMatrix < ActiveRecord::Migration
  def up
    rename_column :reports, :time_distance_matrix,:average_time_distance_matrix_unstressed
    add_column :reports, :average_time_distance_matrix_stressed, :text 
  end
  
  def down
    rename_column :reports, :average_time_distance_matrix_unstressed,:time_distance_matrix
    drop_column :reports, :average_time_distance_matrix_stressed
  end
end
