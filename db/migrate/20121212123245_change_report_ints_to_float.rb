class ChangeReportIntsToFloat < ActiveRecord::Migration
  def up
    change_column :reports, :correction_count_unstressed, :float
    change_column :reports, :correction_count_stressed, :float

    change_column :reports, :levenshtein_distance_unstressed, :float
    change_column :reports, :levenshtein_distance_stressed, :float
  end

  def down
    change_column :reports, :correction_count_unstressed, :integer
    change_column :reports, :correction_count_stressed, :integer

    change_column :reports, :levenshtein_distance_unstressed, :integer
    change_column :reports, :levenshtein_distance_stressed, :integer
  end
end
