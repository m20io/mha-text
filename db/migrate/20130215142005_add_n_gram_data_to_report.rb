class AddNGramDataToReport < ActiveRecord::Migration
  def change
    add_column :reports, :ngram_data, :text
  end
end
