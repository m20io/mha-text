class RenameUserIdColumnOfKeyStroke < ActiveRecord::Migration
  def up
    rename_column :key_strokes, :user_id_id, :user_id
  end

  def down
    rename_column :key_strokes, :user_id, :user_id_id
  end
end
