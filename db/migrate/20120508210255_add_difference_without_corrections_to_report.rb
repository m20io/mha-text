class AddDifferenceWithoutCorrectionsToReport < ActiveRecord::Migration
  def up
    add_column :reports, :count_differences_without_corrections_unstressed, :integer
    add_column :reports, :count_differences_without_corrections_stressed, :integer
  end

  def down
    remove_column :reports, :count_differences_without_corrections_unstressed
    remove_column :reports, :count_differences_without_corrections_stressed
  end
end
