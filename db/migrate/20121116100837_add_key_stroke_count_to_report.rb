class AddKeyStrokeCountToReport < ActiveRecord::Migration
  def change
    add_column :reports, :key_stroke_count_unstressed, :integer
    add_column :reports, :key_stroke_count_stressed, :integer
  end
end
