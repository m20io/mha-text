class AddCountToNGram < ActiveRecord::Migration
  def change
    add_column :n_grams, :count, :integer
  end
end
