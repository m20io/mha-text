class ChangeNgramTableFixSpelling < ActiveRecord::Migration
  def change
    rename_column :n_grams, :timging_data, :timing_data
  end
end
