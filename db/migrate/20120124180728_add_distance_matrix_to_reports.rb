class AddDistanceMatrixToReports < ActiveRecord::Migration
  def change
    add_column :reports, :time_distance_matrix,:text
    add_column :reports, :average_time_distance_stressed, :float
    add_column :reports, :average_time_distance_unstressed, :float
  end
end
