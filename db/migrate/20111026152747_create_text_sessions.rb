class CreateTextSessions < ActiveRecord::Migration
  def change
    create_table :text_sessions do |t|
      t.string :user_id
      t.string :text_1
      t.string :text_2

      t.timestamps
    end
  end
end
