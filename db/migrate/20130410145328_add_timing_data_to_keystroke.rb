class AddTimingDataToKeystroke < ActiveRecord::Migration
  def change
      add_column :key_strokes, :timing_data, :text
  end
end
