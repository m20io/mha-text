class AddWordTimingToReport < ActiveRecord::Migration
  def change
    add_column :reports, :word_timing, :text
  end
end
