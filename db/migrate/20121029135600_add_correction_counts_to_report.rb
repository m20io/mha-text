class AddCorrectionCountsToReport < ActiveRecord::Migration
  def change
    add_column :reports, :correction_count_stressed, :integer
    add_column :reports, :correction_count_unstressed, :integer
  end
end
