class AddPersonalTemplateToTextSession < ActiveRecord::Migration
  def up
    add_column :text_sessions, :personal_template_1, :text
    add_column :text_sessions, :personal_template_2, :text
  end
  def down
    remove_column :text_sessions, :personal_template_1
    remove_column :text_sessions, :personal_template_2
  end
end
