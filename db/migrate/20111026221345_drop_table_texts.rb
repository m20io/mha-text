class DropTableTexts < ActiveRecord::Migration
  def up
    drop_table :texts
  end

  def down
    create_table :texts do |t|
      t.references :user_id
      t.text :final_text

      t.timestamps
    end
  end
end
