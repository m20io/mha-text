class GenerateKeyStrokeIndexes < ActiveRecord::Migration
  def up
    add_index(:key_strokes, [:text_session_id, :position])
    add_index(:key_strokes, [:text_session_id, :time_stamp])
    add_index(:key_strokes, :text_session_id)
    add_index(:key_strokes, :event_type)
    add_index(:key_strokes, :time_stamp)
    add_index(:key_strokes, :position)
  end

  def down
    remove_index(:key_strokes, :text_session_id)
    remove_index(:key_strokes, :event_type)
    remove_index(:key_strokes, :time_stamp)
    remove_index(:key_strokes, :position)
    remove_index(:key_strokes, [:text_session_id, :position])
    remove_index(:key_strokes, [:text_session_id, :time_stamp])
  end
end
