class CreateTexts < ActiveRecord::Migration
  def change
    create_table :texts do |t|
      t.references :user_id
      t.text :final_text

      t.timestamps
    end
    add_index :texts, :user_id_id
  end
end
