# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130421171414) do

  create_table "key_strokes", :force => true do |t|
    t.string   "event_type"
    t.integer  "stroked_key"
    t.boolean  "ctrl_key"
    t.boolean  "alt_key"
    t.boolean  "shift_key"
    t.string   "target"
    t.decimal  "time_stamp",      :precision => 16, :scale => 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "text_session_id"
    t.string   "position"
    t.integer  "up_event_id"
    t.text     "timing_data"
  end

  add_index "key_strokes", ["event_type"], :name => "index_key_strokes_on_event_type"
  add_index "key_strokes", ["position"], :name => "index_key_strokes_on_position"
  add_index "key_strokes", ["text_session_id", "position"], :name => "index_key_strokes_on_text_session_id_and_position"
  add_index "key_strokes", ["text_session_id", "time_stamp"], :name => "index_key_strokes_on_text_session_id_and_time_stamp"
  add_index "key_strokes", ["text_session_id"], :name => "index_key_strokes_on_text_session_id"
  add_index "key_strokes", ["time_stamp"], :name => "index_key_strokes_on_time_stamp"

  create_table "key_strokes_n_grams", :id => false, :force => true do |t|
    t.integer "n_gram_id"
    t.integer "key_stroke_id"
  end

  create_table "n_grams", :force => true do |t|
    t.integer  "text_session_id"
    t.string   "n_gram"
    t.string   "position"
    t.text     "timing_data"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.integer  "count"
  end

  create_table "reports", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "levenshtein_distance_stressed"
    t.float    "levenshtein_distance_unstressed"
    t.integer  "length_difference_stressed"
    t.integer  "length_difference_unstressed"
    t.integer  "text_session_id"
    t.text     "average_time_distance_matrix_unstressed"
    t.float    "average_time_distance_stressed"
    t.float    "average_time_distance_unstressed"
    t.text     "average_time_distance_matrix_stressed"
    t.integer  "count_differences_without_corrections_unstressed"
    t.integer  "count_differences_without_corrections_stressed"
    t.float    "correction_count_stressed"
    t.float    "correction_count_unstressed"
    t.integer  "key_stroke_count_unstressed"
    t.integer  "key_stroke_count_stressed"
    t.integer  "text_ending_index_stressed"
    t.integer  "text_ending_index_unstressed"
    t.text     "key_stroke_counts"
    t.text     "word_timing"
    t.text     "ngram_data"
    t.text     "histogram_us"
    t.text     "histogram_s"
  end

  create_table "text_sessions", :force => true do |t|
    t.string   "user_id"
    t.string   "text_1"
    t.string   "text_2"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "final_text_1"
    t.text     "final_text_2"
    t.text     "stroked_text_1"
    t.text     "stroked_text_2"
    t.text     "personal_template_1"
    t.text     "personal_template_2"
  end

end
