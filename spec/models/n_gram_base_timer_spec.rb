## encoding: utf-8
require 'spec_helper'

describe BaseTimer do
  include Helpers
  before(:each) do
    @text_session = FactoryGirl.create(:text_session,
        :final_text_1 => TextSession.texts['Text A'],
        :final_text_2 => TextSession.texts['Text B'] )
    @report = Report.build_from_text_session(@text_session)

    generate_key_strokes_for_text("en interesting String", @text_session.id, "1")
    generate_key_strokes_for_text("An other String", @text_session.id, "2")

    @report.save
    @text_session.save
  end

  it "should have the correct key_strokes" do
    @text_session.key_strokes.where(position:1).count.should eql 44
    @text_session.key_strokes.where(position:2).count.should eql 34
  end

  it "should count the number of two grams" do

    base_timer = NGramBaseTimer.new(@text_session, @report)

    base_timer.count_key_stroke_sequences("in", 1).should eql 3.to_f
    base_timer.count_key_stroke_sequences("yz", 1).should eql 0.to_f
    base_timer.count_key_stroke_sequences("in", 2).should eql 1.to_f
  end

  it "should count the number of three grams" do

    base_timer = NGramBaseTimer.new(@text_session, @report)

    base_timer.count_key_stroke_sequences("ing", 1).should eql 2.to_f
    base_timer.count_key_stroke_sequences("xyz", 1).should eql 0.to_f
    base_timer.count_key_stroke_sequences("ing", 2).should eql 1.to_f
  end

  it "should count the number of three grams" do

    base_timer = NGramBaseTimer.new(@text_session, @report)

    base_timer.count_key_stroke_sequences("ing", 1).should eql 2.to_f
    base_timer.count_key_stroke_sequences("xyz", 1).should eql 0.to_f
    base_timer.count_key_stroke_sequences("ing", 2).should eql 1.to_f

  end

  it "should calculate the press to release time for two grams with an other method" do
    base_timer = NGramBaseTimer.new(@text_session, @report)

    base_timer.measure_ngram_p2r("in",1).should eql 3 * (2 * 200 + 1 * 1000)
    base_timer.measure_ngram_p2r("yz",1).should eql 0
    base_timer.measure_ngram_p2r("in",2).should eql 1 * (2 * 200 + 1 * 1000)
  end

  it "should measure the multiple n-gram p2r time" do
    base_timer = NGramBaseTimer.new(@text_session, @report)
    base_timer.measure_multi_ngram_p2r(["ing","ter"],1).should eql 3 * (3 * 200 + 2 * 1000)
  end

  it "should count the multiple n-grams" do
    base_timer = NGramBaseTimer.new(@text_session, @report)
    base_timer.count_multi_ngram(["ing","ter"],1).should eql 3.to_f
  end

  it "should calculate the press to release time for two grams with an other method" do
    base_timer = NGramBaseTimer.new(@text_session, @report)

    base_timer.measure_ngram_r2p("in",1).should eql 3 * (1 * 1000)
    base_timer.measure_ngram_r2p("yz",1).should eql 0
    base_timer.measure_ngram_r2p("in",2).should eql 1 * (1 * 1000)
  end

  it "should measure the multiple n-gram r2p time" do
    base_timer = NGramBaseTimer.new(@text_session, @report)
    base_timer.measure_multi_ngram_r2p(["ing","ter"],1).should eql 3 * (1 * 200 + 2 * 1000)
  end

  it "should calculate the press to release time of the first key stroke" do
    base_timer = NGramBaseTimer.new(@text_session, @report)

    base_timer.measure_ngram_p2r_1("in",1).should eql 3 * (1 * 200)
    base_timer.measure_ngram_p2r_1("yz",1).should eql 0
    base_timer.measure_ngram_p2r_1("in",2).should eql 1 * (1 * 200)
  end

  it "should calculate the release to press time of the first key stroke" do
    base_timer = NGramBaseTimer.new(@text_session, @report)

    base_timer.measure_ngram_r2p_1("in",1).should eql 3 * (1 * 1000)
    base_timer.measure_ngram_r2p_1("yz",1).should eql 0
    base_timer.measure_ngram_r2p_1("in",2).should eql 1 * (1 * 1000)
  end

end