## encoding: utf-8
require 'spec_helper'

describe KeyStrokeHistogram do
  include Helpers
  before(:each) do
    @text_session = FactoryGirl.create(:text_session,
        :final_text_1 => TextSession.texts['Text A'],
        :final_text_2 => TextSession.texts['Text B'] )
    @report = Report.build_from_text_session(@text_session)

    @report.save
    @text_session.save
  end

  it "should run analyse and create both histograms" do
    generate_key_strokes_for_text("beispiel", @text_session.id, 1)
    generate_key_strokes_for_text("beispiel", @text_session.id, 2)

    KeyStrokeHistogram.analyse(@text_session,@report)
    hist = Array.new(20,0)
    hist[1] = 7
    @report.histogram_us.should eql hist
  end


  it "should create an histogram" do
    generate_key_strokes_for_text("beispiel", @text_session.id, 1)

    ksh = KeyStrokeHistogram.new(@text_session,@report)
    hist = Array.new(20,0)
    hist[1] = 7
    ksh.create_histogram(1).should eql(hist)
  end


  it "should create an correct histogram for big gaps" do
    generate_key_strokes_for_text("beispiel", @text_session.id, 1)

    down_keys = @text_session.key_strokes.where(event_type: 'keydown')

    down_keys[0].time_stamp += -10000
    down_keys[0].save

    5.times.each do |i|
      down_keys[i+2].time_stamp = down_keys[i+1].time_stamp + 10
      down_keys[i+2].save
    end

    ksh = KeyStrokeHistogram.new(@text_session,@report)
    hist = Array.new(10,0)
    hist[1] = 1
    hist[6] = 1
    ksh.create_histogram(1).should eql hist
  end
end