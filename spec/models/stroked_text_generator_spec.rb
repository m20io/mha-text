require 'spec_helper'

describe StrokedTextGenerator do

  context "stroked text" do
    before(:each) do
      @text_session = FactoryGirl.create(:text_session,
        :final_text_1 => TextSession.texts['Text A'],
        :final_text_2 => TextSession.texts['Text B'] )
    end

    it "should assign to the correct variables" do
      FactoryGirl.create(:up, :stroked_key => 65, :text_session_id => @text_session.id ,position: 1)
      FactoryGirl.create(:down, :stroked_key => 65, :text_session_id => @text_session.id ,position: 1)
      FactoryGirl.create(:up, :stroked_key => 66, :text_session_id => @text_session.id ,position: 1)
      FactoryGirl.create(:down, :stroked_key => 66, :text_session_id => @text_session.id ,position: 1)

      FactoryGirl.create(:up, :stroked_key => 65, :text_session_id => @text_session.id ,position: 2)
      FactoryGirl.create(:down, :stroked_key => 65, :text_session_id => @text_session.id ,position: 2)

      StrokedTextGenerator.analyse(@text_session)

      @text_session.stroked_text_1.should eql "ab"
      @text_session.stroked_text_2.should eql "a"
    end

    it "should translate normale key_stroked into a text" do
      FactoryGirl.create(:up, :stroked_key => 65, :text_session_id => @text_session.id)
      FactoryGirl.create(:down, :stroked_key => 65, :text_session_id => @text_session.id)
      FactoryGirl.create(:up, :stroked_key => 66, :text_session_id => @text_session.id)
      FactoryGirl.create(:down, :stroked_key => 66, :text_session_id => @text_session.id)

      FactoryGirl.create(:up, :stroked_key => 32, :text_session_id => @text_session.id)
      FactoryGirl.create(:down, :stroked_key => 32, :text_session_id => @text_session.id)

      FactoryGirl.create(:up, :stroked_key => 57, :text_session_id => @text_session.id)
      FactoryGirl.create(:down, :stroked_key => 57, :text_session_id => @text_session.id)

      StrokedTextGenerator.analyse(@text_session)

      @text_session.stroked_text_1.should eql "ab 9"
    end

    it "should translate special key_strokes into text" do
      FactoryGirl.create(:up, :stroked_key => 37, :text_session_id => @text_session.id)
      FactoryGirl.create(:down, :stroked_key => 37, :text_session_id => @text_session.id)
      FactoryGirl.create(:up, :stroked_key => 38, :text_session_id => @text_session.id)
      FactoryGirl.create(:down, :stroked_key => 38, :text_session_id => @text_session.id)
      FactoryGirl.create(:up, :stroked_key => 40, :text_session_id => @text_session.id)
      FactoryGirl.create(:down, :stroked_key => 40, :text_session_id => @text_session.id)

      FactoryGirl.create(:up, :stroked_key => 8, :text_session_id => @text_session.id)
      FactoryGirl.create(:down, :stroked_key => 8, :text_session_id => @text_session.id)

      StrokedTextGenerator.analyse(@text_session)

      @text_session.stroked_text_1.should eql "#left##up##down##backspace#"
    end

    it "should translate capital letters" do
      FactoryGirl.create(:up, :stroked_key => 65, :text_session_id => @text_session.id,
        :shift_key => true)
      FactoryGirl.create(:down, :stroked_key => 65, :text_session_id => @text_session.id,
        :shift_key => true)
      FactoryGirl.create(:up, :stroked_key => 66, :text_session_id => @text_session.id,
        :shift_key => true)
      FactoryGirl.create(:down, :stroked_key => 66, :text_session_id => @text_session.id,
        :shift_key => true)

      StrokedTextGenerator.analyse(@text_session)

      @text_session.stroked_text_1.should eql "AB"
    end
  end

end