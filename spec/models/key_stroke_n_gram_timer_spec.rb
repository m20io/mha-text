## encoding: utf-8
require 'spec_helper'

describe KeyStrokeNGramTimer do
  include Helpers
  before(:each) do
    @text_session = FactoryGirl.create(:text_session,
        :final_text_1 => TextSession.texts['Text A'],
        :final_text_2 => TextSession.texts['Text B'] )
    @report = Report.build_from_text_session(@text_session)

    generate_key_strokes_for_text("en interesting String", @text_session.id, "1")
    generate_key_strokes_for_text("An other String", @text_session.id, "2")

    @report.save
    @text_session.save

  end

  it "should assign the proper values to a report" do
    KeyStrokeNGramTimer.analyse(@text_session, @report)
    @report.ngram_data[:common_digram_p2r_stressed].should eql 1400.0
    @report.ngram_data[:common_digram_p2r_unstressed].should eql 1400.0
    @report.ngram_data[:common_digram_r2p_stressed].should eql 1000.0
    @report.ngram_data[:common_digram_r2p_unstressed].should eql 1000.0

    @report.ngram_data[:common_inter_digram_p2r_stressed].should eql 0.0
    @report.ngram_data[:common_inter_digram_p2r_unstressed].should eql 1400.0
    @report.ngram_data[:common_inter_digram_r2p_stressed].should eql 0.0
    @report.ngram_data[:common_inter_digram_r2p_unstressed].should eql 1000.0

    @report.ngram_data[:common_intra_digram_p2r_stressed].should eql 1400.0
    @report.ngram_data[:common_intra_digram_p2r_unstressed].should eql 1400.0
    @report.ngram_data[:common_intra_digram_r2p_stressed].should eql 1000.0
    @report.ngram_data[:common_intra_digram_r2p_unstressed].should eql 1000.0


    @report.ngram_data[:common_digram_p2r_1_stressed].should eql 200.0
    @report.ngram_data[:common_digram_p2r_1_unstressed].should eql 200.0

    @report.ngram_data[:common_inter_digram_p2r_1_stressed].should eql 0.0
    @report.ngram_data[:common_inter_digram_p2r_1_unstressed].should eql 200.0


    @report.ngram_data[:common_intra_digram_p2r_1_stressed].should eql 200.0
    @report.ngram_data[:common_intra_digram_p2r_1_unstressed].should eql 200.0
  end

  it "should calculate for common bi and tri grams" do
    KeyStrokeNGramTimer.analyse(@text_session, @report)
    @report.ngram_data[:ngram_in_p2r_unstressed].should eql 1400.0
    @report.ngram_data[:ngram_in_p2r_stressed].should eql 1400.0
    @report.ngram_data[:ngram_in_r2p_unstressed].should eql 1400.0
    @report.ngram_data[:ngram_in_r2p_stressed].should be_within(1).of(466)
  end

  # it "should calculate for all letter the first bigram" do
  #   KeyStrokeNGramTimer.analyse(@text_session, @report)
  #   @report.ngram_data[:bi_gram_t_p2r_1_unstressed].should eql 200.0
  #   @report.ngram_data[:bi_gram_t_p2r_1_stressed].should eql 200.0
  #   @report.ngram_data[:bi_gram_t_r2p_1_unstressed].should eql 1000.0
  #   @report.ngram_data[:bi_gram_t_r2p_1_stressed].should eql 1000.0
  # end

  # it "should calculate for all inter hand bigrams starting with a letter the first time" do
  #   KeyStrokeNGramTimer.analyse(@text_session, @report)
  #   @report.ngram_data[:bi_gram_t_intra_hand_p2r_1_stressed].should eql 1 * 200.0
  #   @report.ngram_data[:bi_gram_t_intra_hand_p2r_1_stressed].should eql 1 * 200.0
  #   @report.ngram_data[:bi_gram_t_intra_hand_r2p_1_stressed].should eql 1 * 1000.0
  #   @report.ngram_data[:bi_gram_t_intra_hand_r2p_1_stressed].should eql 1 * 1000.0
  # end

  # it "should calculate for all intra hand bigrams starting with a letter the first time" do
  #   KeyStrokeNGramTimer.analyse(@text_session, @report)
  #   @report.ngram_data[:bi_gram_t_intra_hand_p2r_1_stressed].should eql 200.0
  #   @report.ngram_data[:bi_gram_t_intra_hand_p2r_1_stressed].should eql 200.0
  #   @report.ngram_data[:bi_gram_t_intra_hand_r2p_1_stressed].should eql 1000.0
  #   @report.ngram_data[:bi_gram_t_intra_hand_r2p_1_stressed].should eql 1000.0
  # end


end