## encoding: utf-8
require 'spec_helper'

describe KeyStrokeTimer do
  include Helpers
  before(:each) do
    @text_session = FactoryGirl.create(:text_session,
        :final_text_1 => TextSession.texts['Text A'],
        :final_text_2 => TextSession.texts['Text B'] )
    @report = Report.build_from_text_session(@text_session)

    @report.save
    @text_session.save
  end

  it "should measure p2r for backspaces" do
    generate_key_strokes_for_text("beii#backspace#spp#backspace#iel", @text_session.id, 1)
    generate_key_strokes_for_text("beispiel", @text_session.id, 2)

    key_stroke_timer = KeyStrokeTimer.new(@text_session,@report)
    result = key_stroke_timer.calculate_average_key_p2r('#backspace#')
    result[0].should be_within(1).of(200)
    result[1].should be_within(1).of(0)
  end

  it "should measure the average p2r time" do
    generate_key_strokes_for_text("beispiel", @text_session.id, 1)
    key_stroke_timer = KeyStrokeTimer.new(@text_session,@report)
    key_stroke_timer.average_p2r_time(1).should be_within(1).of(200)
  end

  it "should measure the average p2r time" do
    generate_key_strokes_for_text("beispiel", @text_session.id, 1)
    key_stroke_timer = KeyStrokeTimer.new(@text_session,@report)
    key_stroke_timer.average_r2p_time(1).should be_within(1).of(875)
  end

  it "should measure the p2r time for common letters" do
    generate_key_strokes_for_text("Beispiel Satze mit einem Punkt.", @text_session.id, 1)

    key_stroke_timer = KeyStrokeTimer.new(@text_session,@report)
    key_stroke_timer.measure_key_group_p2r(["e","n"],1).should eql 200.0
  end

  it "should measure the r2p time for common letters" do
    generate_key_strokes_for_text("Beispiel Satze mit einem Punkt.", @text_session.id, 1)

    key_stroke_timer = KeyStrokeTimer.new(@text_session,@report)
    key_stroke_timer.measure_key_group_r2p(["e","n"],1).should eql 1000.0
    key_stroke_timer.measure_key_group_r2p(["e","n"],1).should eql 1000.0
  end

  it "should analyse all letters" do
    generate_key_strokes_for_text("Beispiel Satze mit einem Punkt.", @text_session.id, 1)

    KeyStrokeTimer.analyse(@text_session,@report)
    ('a'..'z').each do |l|
      @report.key_stroke_counts["letter_#{l}_p2r_unstressed".to_sym].should be_present
    end

  end

  pending "should be fast" do
    generate_key_strokes_for_text("Beispiel Satze mit einem Punkt. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. ", @text_session.id, 1)
    key_stroke_timer = KeyStrokeTimer.new(@text_session,@report)

    # time = Time.now
    # key_group = ["e"]
    # @text_session.key_strokes.where(:position => 1)
    # .where(:stroked_key => key_group.map{|key| KeyStroke.keymap[key]}).order(:time_stamp)
    # print "key_stroke query for 1 letter did take #{Time.now - time}\n"

    # time = Time.now
    # key_group = ["e","n"]
    # key_strokes = @text_session.key_strokes.where(:position => 1).
    #   where(:stroked_key => key_group.map{|key| KeyStroke.keymap[key]}).order(:time_stamp)
    # print "key_stroke query for 2 letter did take #{Time.now - time}\n"

    @text_session.key_strokes.each{|ks| ks.measure_timing_data}

    time = Time.now
    key_stroke_timer.measure_key_group_r2p(["e"],1).should eql 1000.0
    print "r2p for 1 letters did take #{Time.now - time}\n"

    time = Time.now
    key_stroke_timer.measure_key_group_r2p(["e","n"],1).should eql 1000.0
    print "r2p for 2 letters did take #{Time.now - time}\n"
  end
end