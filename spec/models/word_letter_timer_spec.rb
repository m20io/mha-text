## encoding: utf-8
require 'spec_helper'

describe WordLetterTimer do
  include Helpers

  before(:each) do
    @text_session = FactoryGirl.create(:text_session)
  end

  it "should assin the values to an report" do
    generate_key_strokes_for_text("Hello World and Martin", @text_session.id, 1)
    generate_key_strokes_for_text("Thank god its Friday", @text_session.id, 2)
    report = Report.build_from_text_session(@text_session)
    WordLetterTimer.analyse(@text_session,report)

    report.capital_case_starting_letter_p2r_unstressed.should be_within(1).of(200)
    report.capital_case_starting_letter_p2r_stressed.should be_within(1).of(200)
    report.capital_case_starting_letter_r2p_unstressed.should be_within(1).of(1000)
    report.capital_case_starting_letter_r2p_stressed.should be_within(1).of(1000)

    report.lower_case_starting_letter_p2r_unstressed.should be_within(1).of(200)
    report.lower_case_starting_letter_p2r_stressed.should be_within(1).of(200)
    report.lower_case_starting_letter_r2p_unstressed.should be_within(1).of(1000)
    report.lower_case_starting_letter_r2p_stressed.should be_within(1).of(1000)

    report.in_word_letter_p2r_unstressed.should be_within(1).of(200)
    report.in_word_letter_p2r_stressed.should be_within(1).of(200)
    report.in_word_letter_r2p_unstressed.should be_within(1).of(933) # last one has no time
    report.in_word_letter_r2p_stressed.should be_within(1).of(923)   # last one has no time

    report.inter_word_fly_time_unstressed.should be_within(1).of(1000)
    report.inter_word_fly_time_stressed.should be_within(1).of(1000)
  end

  it "should count the number of capital starting letter" do
    generate_key_strokes_for_text(
      "Hello World and Martin, the House of Cards and somebody else", @text_session.id, 1)
    wlt = WordLetterTimer.new(@text_session, Report.build_from_text_session(@text_session))
    wlt.count_capital_starting_letter("1").should eql (5 + 0.00001)
  end

  it "should count the number of lower case starting letter" do
    generate_key_strokes_for_text(
      "Hello World and Martin, the House of Cards and somebody else today", @text_session.id, 1)
    wlt = WordLetterTimer.new(@text_session, Report.build_from_text_session(@text_session))
    wlt.count_lower_case_starting_letter("1").should eql (7 + 0.00001)
  end

  it "should count the number of lower case in word letter" do
    generate_key_strokes_for_text(
      "Hello World and martin", @text_session.id, 1)
    wlt = WordLetterTimer.new(@text_session, Report.build_from_text_session(@text_session))
    wlt.count_lower_case_in_word_letter("1").should eql (15 + 0.00001)
  end

  it "should measure the p2r time of capital starting letter" do
    generate_key_strokes_for_text(
      "Hello World and Martin, the House of Cards and somebody else", @text_session.id, 1)
    wlt = WordLetterTimer.new(@text_session, Report.build_from_text_session(@text_session))
    wlt.measure_capital_starting_letter_p2r("1").should eql 5 * 200
  end

  it "should measure the r2p time of capital starting letter" do
    generate_key_strokes_for_text(
      "Hello World and Martin, the House of Cards and somebody else", @text_session.id, 1)
    wlt = WordLetterTimer.new(@text_session, Report.build_from_text_session(@text_session))
    wlt.measure_capital_starting_letter_r2p("1").should eql 5 * 1000
  end

  it "should measure the r2p time and recognize overlapps" do
    generate_key_strokes_for_text("Hello", @text_session.id, 1)
    # #shift# 0,1 H 2,3 e 4,5 => 3 > 4
    @text_session.key_strokes[4].time_stamp = @text_session.key_strokes[3].time_stamp - 100
    @text_session.key_strokes[4].save
    @text_session.key_strokes.map(&:measure_timing_data)

    wlt = WordLetterTimer.new(@text_session, Report.build_from_text_session(@text_session))
    wlt.measure_capital_starting_letter_r2p("1").should eql 1 * 1000 - 1000
  end

  it "should measure the p2r time of lower case starting letter" do
    generate_key_strokes_for_text(
      "Hello World and Martin, the House of Cards and somebody else", @text_session.id, 1)
    wlt = WordLetterTimer.new(@text_session, Report.build_from_text_session(@text_session))
    wlt.measure_lower_case_starting_letter_p2r("1").should eql 6 * 200
  end

  it "should measure the r2p time of lower case starting letter" do
    generate_key_strokes_for_text(
      "Hello World and Martin, the House of Cards and somebody else", @text_session.id, 1)
    wlt = WordLetterTimer.new(@text_session, Report.build_from_text_session(@text_session))
    wlt.measure_lower_case_starting_letter_r2p("1").should eql 6 * 1000
  end

  it "should measure the p2r time of in word letter" do
    generate_key_strokes_for_text(
      "Hello World and martin", @text_session.id, 1)
    wlt = WordLetterTimer.new(@text_session, Report.build_from_text_session(@text_session))
    wlt.measure_in_word_letter_p2r("1").should eql 15 * 200
  end

  it "should measure the r2p time of in word letter" do
    generate_key_strokes_for_text(
      "Hello World and martin", @text_session.id, 1)
    wlt = WordLetterTimer.new(@text_session, Report.build_from_text_session(@text_session))
    wlt.measure_in_word_letter_r2p("1").should eql 15 * 1000 - 1000 #last one don't count
  end
  it "should measure not special keys" do
    generate_key_strokes_for_text(
      "Hello World+ and, martin", @text_session.id, 1)
    wlt = WordLetterTimer.new(@text_session, Report.build_from_text_session(@text_session))
    wlt.measure_in_word_letter_r2p("1").should eql 15 * 1000 - 1000 #last one don't count
  end


end