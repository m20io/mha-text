# == Schema Information
#
# Table name: text_sessions
#
#  id                  :integer          not null, primary key
#  user_id             :string(255)
#  text_1              :string(255)
#  text_2              :string(255)
#  created_at          :datetime
#  updated_at          :datetime
#  final_text_1        :text
#  final_text_2        :text
#  stroked_text_1      :text
#  stroked_text_2      :text
#  personal_template_1 :text
#  personal_template_2 :text
#

require 'spec_helper'

describe TextSession do
  
  context "selection" do
    it "should know which text_sessions are relevant " do
      
      FactoryGirl.create(:text_session,
        :final_text_1 => TextSession.texts['Text A'],
        :final_text_2 => TextSession.texts['Text B'] )
      FactoryGirl.create(:text_session)

      TextSession.proper_filled.count.should eql 1
    end
  end

end
