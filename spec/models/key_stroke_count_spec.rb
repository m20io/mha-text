## encoding: utf-8
require 'spec_helper'

describe KeyStrokeCounter do
  before(:each) do
    @text_session = FactoryGirl.create(:text_session,
        :final_text_1 => TextSession.texts['Text A'],
        :final_text_2 => TextSession.texts['Text B'] )
    @report = Report.build_from_text_session(@text_session)
    5.times do
      FactoryGirl.create(:up, :stroked_key => 65, :text_session_id => @text_session.id ,position: 1)
      FactoryGirl.create(:down, :stroked_key => 65, :text_session_id => @text_session.id ,position: 1)
      FactoryGirl.create(:up, :stroked_key => 66, :text_session_id => @text_session.id ,position: 1)
      FactoryGirl.create(:down, :stroked_key => 66, :text_session_id => @text_session.id ,position: 1)

      FactoryGirl.create(:up, :stroked_key => 65, :text_session_id => @text_session.id ,position: 2)
      FactoryGirl.create(:down, :stroked_key => 65, :text_session_id => @text_session.id ,position: 2)
    end
    @report.save
    @text_session.save
  end


  it "should count key strokes" do
    KeyStrokeCounter.analyse(@text_session,@report)

    @report.reload

    @report.key_stroke_count_unstressed.should eql 10
    @report.key_stroke_count_stressed.should eql 5
  end

  it "should count backspaces" do
    #"#backspace#" => "8",

    3.times do
      FactoryGirl.create(:up, :stroked_key => 8, :text_session_id => @text_session.id ,position: 1)
      FactoryGirl.create(:down, :stroked_key => 8, :text_session_id => @text_session.id ,position: 1)
    end

    FactoryGirl.create(:up, :stroked_key => 8, :text_session_id => @text_session.id ,position: 2)
    FactoryGirl.create(:down, :stroked_key => 8, :text_session_id => @text_session.id ,position: 2)

    KeyStrokeCounter.analyse(@text_session,@report)

    @report.reload

    @report.backspace_count_unstressed.should eql 3
    @report.backspace_count_stressed.should eql 1
  end

  it "should count arrows" do
      # "#left#" => "37",
      # "#up#" => "38",
      # "#right#" => "39",
      # "#down#" => "40",

    3.times do
      FactoryGirl.create(:up, :stroked_key => 37, :text_session_id => @text_session.id ,position: 1)
      FactoryGirl.create(:down, :stroked_key => 37, :text_session_id => @text_session.id ,position: 1)
      FactoryGirl.create(:up, :stroked_key => 40, :text_session_id => @text_session.id ,position: 1)
      FactoryGirl.create(:down, :stroked_key => 40, :text_session_id => @text_session.id ,position: 1)
    end

    FactoryGirl.create(:up, :stroked_key => 40, :text_session_id => @text_session.id ,position: 2)
    FactoryGirl.create(:down, :stroked_key => 40, :text_session_id => @text_session.id ,position: 2)

    KeyStrokeCounter.analyse(@text_session,@report)

    @report.reload

    @report.arrow_count_unstressed.should eql 6
    @report.arrow_count_stressed.should eql 1
  end


end