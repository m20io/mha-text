# encoding: utf-8
require 'spec_helper'

describe KeyStrokeAssignment do
  include Helpers

  before(:each) do
    @text_session = FactoryGirl.create(:text_session)
  end

  it "should assign the up event to the down events" do
    generate_key_strokes_for_text("up", @text_session.id, 1)
    ksa = KeyStrokeAssignment.new()
    ksa.assign_key_strokes(@text_session.key_strokes.where(position: 1))
    down = @text_session.key_strokes.where(position: 1)[0]
    up = @text_session.key_strokes.where(position: 1)[1]

    down.up_event.should eql up
  end

  it "should assign the up events to the down events proper" do
    generate_key_strokes_for_text("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.", @text_session.id, 1)
    ksa = KeyStrokeAssignment.new()
    ksa.assign_key_strokes(@text_session.key_strokes.where(position: 1))
    @text_session.key_strokes.where(event_type: 'keydown').where("up_event_id IS NULL").should be_blank
  end
end