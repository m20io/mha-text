## encoding: utf-8
require 'spec_helper'


describe WordTimer do
  include Helpers
  before(:each) do
    @text_session = FactoryGirl.create(:text_session)
  end

  it "should measure the average timing of common words" do


  end

  it "should find the key strokes for a word" do
    generate_key_strokes_for_text("Hello word and something more", @text_session.id, 1)

    word_timer = WordTimer.new(@text_session, Report.build_from_text_session(@text_session))

    key_strokes = word_timer.find_key_strokes("word",1,1).first
    key_strokes.size.should eql 8
    key_strokes.first.letter.should eql "w"
    key_strokes.last.letter.should eql "d"
  end

  it "should find the n-th occurrence of a word in the key strokes" do
    generate_key_strokes_for_text("Hello word and something other word", @text_session.id, 1)

    word_timer = WordTimer.new(@text_session, Report.build_from_text_session(@text_session))

    results = word_timer.find_key_strokes("word",1,2)
    results.size.should eql 2

    key_strokes = results.last

    key_strokes.size.should eql 8

    key_strokes.first.letter.should eql "w"
    key_strokes.last.letter.should eql "d"
  end

  it "should return nil if no occurrences exist" do
    generate_key_strokes_for_text("Hello World!", @text_session.id, 1)
    word_timer = WordTimer.new(@text_session, Report.build_from_text_session(@text_session))

    key_storkes = word_timer.find_key_strokes("word",1,1).should eql []
  end

  it "should calculate the average common word timing" do
    generate_key_strokes_for_text("Hello and something common word das der", @text_session.id, 1)
    generate_key_strokes_for_text("Hello and something common word sich zu der", @text_session.id, 2)

    report = Report.build_from_text_session(@text_session)
    WordTimer.analyse(@text_session, report)

    report.common_word_p2r_unstressed.should be_within(1).of(811) # 2
    report.common_word_p2r_stressed.should be_within(1).of(804)

  end
end