## encoding: utf-8
require 'spec_helper'

describe BaseTimer do
  include Helpers
  before(:each) do
    @text_session = FactoryGirl.create(:text_session)
  end

  it "should initilize with a text session and a report" do
    report = Report.build_from_text_session(@text_session)
    base_timer = BaseTimer.new(@text_session,report)

    base_timer.text_session.should eql @text_session
    base_timer.report.should eql report
  end

  it "should count the a letter" do
    generate_key_strokes_for_text("some text", @text_session.id, 1)
    base_timer = BaseTimer.new(@text_session,Report.build_from_text_session(@text_session))

    base_timer.count_letter("e",1).should eql 2.0
    base_timer.count_letter("a",1).should eql 0.00001
  end

  it "should measure the p2r time" do
    generate_key_strokes_for_text("beispiel", @text_session.id, 1)
    base_timer = BaseTimer.new(@text_session,Report.build_from_text_session(@text_session))
    base_timer.measure_key_stroke_p2r_time(@text_session.key_strokes.where(event_type: 'keydown'),"1").
      should eql 8 * 200
  end

  it "should measure the p2r time of backspaces" do
    generate_key_strokes_for_text("beii#backspace#spiel", @text_session.id, 1)

    base_timer = BaseTimer.new(@text_session,Report.build_from_text_session(@text_session))
    backspaces = @text_session.key_strokes.where(stroked_key: KeyStroke.keymap['#backspace#']).
      where(event_type: "keydown")

    base_timer.measure_key_stroke_p2r_time(backspaces,"1").
      should eql 1 * 200
  end

  it "should measer the r2p time" do
    generate_key_strokes_for_text("beispiel", @text_session.id, 1)

    base_timer = BaseTimer.new(@text_session,Report.build_from_text_session(@text_session))
    base_timer.measure_key_stroke_r2p_time(@text_session.key_strokes.where(event_type: 'keydown'),"1").

      should eql 7 * 1000
  end
end