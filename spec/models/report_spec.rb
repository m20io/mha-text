# encoding: utf-8
# == Schema Information
#
# Table name: reports
#
#  id                                               :integer          not null, primary key
#  created_at                                       :datetime
#  updated_at                                       :datetime
#  levenshtein_distance_stressed                    :integer
#  levenshtein_distance_unstressed                  :integer
#  length_difference_stressed                       :integer
#  length_difference_unstressed                     :integer
#  text_session_id                                  :integer
#  average_time_distance_matrix_unstressed          :text
#  average_time_distance_stressed                   :float
#  average_time_distance_unstressed                 :float
#  average_time_distance_matrix_stressed            :text
#  count_differences_without_corrections_unstressed :integer
#  count_differences_without_corrections_stressed   :integer
#  correction_count_stressed                        :integer
#  correction_count_unstressed                      :integer
#

require 'spec_helper'

describe Report do
  context "Creation" do
    it "should know how to be created from a text session" do

      text_session = FactoryGirl.create(:text_session)
      report = Report.build_from_text_session(text_session)

      report.class.should eql Report
      report.should be_valid
    end
  end
  context "Validation" do
    let (:report) { FactoryGirl.build(:report) }

    it "should be not valid without a text session" do
      report.text_session = nil
      report.should_not be_valid
    end

    it "should be valid otherwise" do
      report.should be_valid
    end
  end

  context "Storing" do
    let (:report) { FactoryGirl.build(:report) }
    it "should store the correction count" do
      report.correction_count_unstressed = 5
      report.correction_count_stressed = 10

      report.save
      report.reload

      report.correction_count_unstressed.should eql 5.0
      report.correction_count_stressed.should eql 10.0
    end
  end
  context "Display" do
    let (:report) { FactoryGirl.build(:report) }

    it "should display a group class depending on the user_id" do
      report.text_session.user_id = "SomeingG1"
      report.group.should eql "group1"
      report.text_session.user_id = "SomeingG2"
      report.group.should eql "group2"
      report.text_session.user_id = "SomeingG4"
      report.group.should eql "group4"
    end
  end

  context "csv export" do
    it "should export all reports" do
      5.times { FactoryGirl.create(:report) }
      reports = Report.order(:id)

      reports.first.levenshtein_distance_stressed = 27
      reports.first.save

      csv_data = reports.generate_csv
      csv_data.should include(reports.first.text_session.user_id)
      csv_data.should include("27,0")
    end
  end
end
