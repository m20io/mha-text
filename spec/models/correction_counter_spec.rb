# encoding: utf-8

require 'spec_helper'

describe CorrectionCounter do
  context "checking regexp" do
    # /(#backspace#|#BACKSPACE#)/
    it "should count backspaces correctly" do
      text = "Hello Wolrd! #backspace##backspace# and large #BACKSPACE#"
      cc = CorrectionCounter.new
      cc.send(:count_correction,text).should eql 3
    end

    # /(#delete#|#DELETE#)/
    it "should count deletes" do
      text = "Hello Wolrd! #left##left##delete##delete#."
      cc = CorrectionCounter.new
      cc.send(:count_correction,text).should eql 2
    end

    # /((#LEFT#|#RIGHT#|#UP#|#DOWN#)+(\w))/
    it "should count marking the text and pressing any word char key afterwords" do
      text = "Hello Wolrd! #LEFT##RIGHT# and have fun!"
      cc = CorrectionCounter.new
      cc.send(:count_correction,text).should eql 2
    end
  end

  context "analysing" do
    it "should count the correction in the stored text" do
      text_session = FactoryGirl.create(:text_session)
      text_session.stroked_text_1 = "Hello World #backspace#!"
      text_session.stroked_text_2 = "Hello World #backspace##backspace#!"
      report = Report.build_from_text_session(text_session)
      report.key_stroke_count_unstressed = 10
      report.key_stroke_count_stressed = 10

      CorrectionCounter.analyse(report.text_session,report)

      report.correction_count_unstressed.should eql 1.0
      report.correction_count_stressed.should eql 2.0

    end
  end


end
