## encoding: utf-8
require 'spec_helper'

describe BaseTimer do
  include Helpers
  before(:each) do
    @text_session = FactoryGirl.create(:text_session)
  end

  it "should create all bi grams" do
    generate_key_strokes_for_text("some nice text",@text_session.id,"1",false)
    ngram_gen = NGramGenerator.new(@text_session,Report.build_from_text_session(@text_session))

    bigrams = ngram_gen.create_all_bi_grams("1")
    bigrams.size.should eql 9
  end

  it "should count bi grams only once" do
    generate_key_strokes_for_text("bi bi",@text_session.id,"1",false)
    ngram_gen = NGramGenerator.new(@text_session,Report.build_from_text_session(@text_session))

    bigrams = ngram_gen.create_all_bi_grams("1")
    bigrams.size.should eql 1
    bigrams.keys.first.should eql "bi"
  end

  it "should ignore capital letters" do
    generate_key_strokes_for_text("Bi Bi",@text_session.id,"1",false)
    ngram_gen = NGramGenerator.new(@text_session,Report.build_from_text_session(@text_session))

    bigrams = ngram_gen.create_all_bi_grams("1")
    bigrams.should be_empty
  end

  it "should measure the average p2r time" do
    generate_key_strokes_for_text("so so nice text",@text_session.id,"1",false)
    ngram_gen = NGramGenerator.new(@text_session,Report.build_from_text_session(@text_session))

    bigrams = ngram_gen.create_all_bi_grams("1")
    bigrams.values.first.average_p2r.should eql 1400.0
  end

  it "should create all tri grams" do
    generate_key_strokes_for_text("some nice text",@text_session.id,"1",false)
    ngram_gen = NGramGenerator.new(@text_session,Report.build_from_text_session(@text_session))

    trigrams = ngram_gen.create_all_tri_grams("1")
    trigrams.size.should eql 6
  end

  it "should run analyse proper" do
    generate_key_strokes_for_text("some nice text",@text_session.id,"1",false)
    NGramGenerator.analyse(@text_session,Report.build_from_text_session(@text_session))
    @text_session.reload
    @text_session.n_grams.size.should eql 9 + 6
  end

end
