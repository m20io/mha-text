# == Schema Information
#
# Table name: key_strokes
#
#  id              :integer          not null, primary key
#  event_type      :string(255)
#  stroked_key     :integer
#  ctrl_key        :boolean
#  alt_key         :boolean
#  shift_key       :boolean
#  target          :string(255)
#  time_stamp      :integer
#  created_at      :datetime
#  updated_at      :datetime
#  text_session_id :integer
#  position        :string(255)
#

## encoding: utf-8
require 'spec_helper'

describe KeyStroke do
  include Helpers
  context "letter encoding", :focus => true do
    it "should should return the lower case version if the shift key is false " do
      small_t = FactoryGirl.create(:down_84)
      small_t.letter.should eql "t"
    end

    it "should return the upper case verion of a letter if the shift_key is true" do
      large_t = FactoryGirl.create(:down_84, :shift_key => true)
      large_t.letter.should eql "T"
    end

    it "should work with special key like space" do
      space = FactoryGirl.create(:down_32)
      space.letter.should eql " "
    end

    it "should create a text pattern for unkown codes" do
      unkown_code = FactoryGirl.create(:down, stroked_key: 1000)
      unkown_code.letter.should eql '#KEY1000#'
    end
  end
  context "timing data" do

    it "should measure the r2p time" do
      text_session = FactoryGirl.create(:text_session)
      generate_key_strokes_for_text("Hello", text_session.id, 1)

      text_session.key_strokes[2].measure_timing_data
      text_session.key_strokes[2].r2p.should eql 1000
    end

    it "should recognise overlapps while r2p measuring" do
      text_session = FactoryGirl.create(:text_session)
      generate_key_strokes_for_text("Hello", text_session.id, 1)
      # #shift# 0,1 H 2,3 e 4,5 => 3 > 4
      text_session.key_strokes[4].time_stamp = text_session.key_strokes[3].time_stamp - 100
      text_session.key_strokes[4].save
      text_session.key_strokes[2].measure_timing_data
      text_session.key_strokes[2].r2p.should eql 0
    end
  end
end
