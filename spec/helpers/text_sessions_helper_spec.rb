require 'spec_helper'

# Specs in this file have access to a helper object that includes
# the TextSessionsHelper. For example:
#
# describe TextSessionsHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       helper.concat_strings("this","that").should == "this that"
#     end
#   end
# end
describe TextSessionsHelper do
  it "should highlight a match with <span class='match'></span>" do
    text = "some text match some other text"
    highlighted_text = highlight_regexp(text, [/(match)/])
    highlighted_text.should eql "some text <span class='match'>match</span> some other text"
  end
end
