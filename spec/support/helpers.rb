module Helpers

  def generate_key_strokes_for_text(text, text_session_id, position, generate_n_grams = true)
    key_strokes = []
    timestamp = Time.now.to_i
    # to give it 13 digests
    timestamp *= 1000

    letters = []
    parts = text.split(/(#\w+#)/)
    parts.each do |part|
      if part.include?("#")
        letters << part
      else
        letters << part.split(//)
      end
    end

    letters.flatten!

    letters.each do |letter|
      if letter == letter.upcase && letter.upcase != letter.downcase
        timestamp += 1000
        down = FactoryGirl.create(:down, :stroked_key => 16, :text_session_id => text_session_id ,
          position: position, time_stamp: timestamp, shift_key: true )
        key_strokes << down

        timestamp += 200

        up = FactoryGirl.create(:up, :stroked_key => 16, :text_session_id => text_session_id ,
          position: position, time_stamp: timestamp, shift_key: true)
        down.up_event_id = up.id
        down.save
        key_strokes << up
      end

      timestamp += 1000
      down = FactoryGirl.create(:down, :stroked_key => KeyStroke.keymap[letter.downcase], :text_session_id => text_session_id ,
        position: position, time_stamp: timestamp, shift_key: letter == letter.upcase )
      key_strokes << down

      timestamp += 200

      up = FactoryGirl.create(:up, :stroked_key => KeyStroke.keymap[letter.downcase], :text_session_id => text_session_id ,
        position: position, time_stamp: timestamp, shift_key: letter == letter.upcase)
      down.up_event_id = up.id
      down.save
      key_strokes << up
    end
    key_strokes.map(&:measure_timing_data)

    ngg = NGramGenerator.new(TextSession.find(text_session_id),nil)
    ngg.create_all_n_grams(position) if generate_n_grams

    key_strokes

  end

  def generate_a_key_strokes(text_session)
    5.times do |i|
      FactoryGirl.create(:down, :stroked_key => 65, :text_session_id => text_session.id ,
        position: 1, time_stamp: [1000000000,100].join.to_i + i * 2000 )
      FactoryGirl.create(:up, :stroked_key => 65, :text_session_id => text_session.id ,
        position: 1, time_stamp: [1000000000,500].join.to_i + i * 2000 )

      FactoryGirl.create(:down, :stroked_key => 66, :text_session_id => text_session.id ,
        position: 1, time_stamp: [1000000001,100].join.to_i + i * 2000)
      FactoryGirl.create(:up, :stroked_key => 66, :text_session_id => text_session.id ,
        position: 1, time_stamp: [1000000001,500].join.to_i + i * 2000)

      FactoryGirl.create(:down, :stroked_key => 65, :text_session_id => text_session.id ,
        position: 2, time_stamp: [1000001002,100].join.to_i + i * 2000)
      FactoryGirl.create(:up, :stroked_key => 65, :text_session_id => text_session.id ,
        position: 2, time_stamp: [1000001002,500].join.to_i + i * 2000)
    end
  end

end