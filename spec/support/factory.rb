FactoryGirl.define do
  factory :text_session do
    sequence(:user_id)         { |n| "user_id_#{n}" }
  end

  factory :up_84, :class => KeyStroke do
    event_type "keyup"
    stroked_key 84
    time_stamp  { ("%10d%03d" % [Time.now.to_i, Time.now.usec.to_s[1..3].to_i]).to_i }
  end

  factory :down_84, :class => KeyStroke do
    event_type "keydown"
    stroked_key 84
    time_stamp  { ("%10d%03d" % [Time.now.to_i, Time.now.usec.to_s[1..3].to_i]).to_i }
  end

  factory :up_32, :class => KeyStroke do
    event_type "keyup"
    stroked_key 32
    time_stamp  { ("%10d%03d" % [Time.now.to_i, Time.now.usec.to_s[1..3].to_i]).to_i }
  end

  factory :down_32, :class => KeyStroke do
    event_type "keydown"
    stroked_key 32
    time_stamp  { ("%10d%03d" % [Time.now.to_i, Time.now.usec.to_s[1..3].to_i]).to_i }
  end


  factory :up, :class => KeyStroke do
    event_type "keyup"
    time_stamp  { ("%10d%03d" % [Time.now.to_i, Time.now.usec.to_s[1..3].to_i]).to_i }
    position 1
  end

  factory :down, :class => KeyStroke do
    event_type "keydown"
    time_stamp  { ("%10d%03d" % [Time.now.to_i, Time.now.usec.to_s[1..3].to_i]).to_i }
    position 1
  end

  factory :report do
    text_session { FactoryGirl.create(:text_session) }
  end

end