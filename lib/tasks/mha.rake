# encoding: UTF-8
namespace :mha do
  desc "Deletes all reports"
  task :delete => :environment do
    init_logger

    Rails.logger.info("Removing all existing reports...")
    Report.delete_all

    Rails.logger.info("... done.")
  end #task

  desc "Runs all analyser and stores there results"
  task :generate => [:environment] do
    init_logger

    TextSession.proper_filled.each do |ts|
      if Report.find_by_text_session_id(ts.id).blank?
        report = Report.build_from_text_session(ts)
        report.save
      end
    end

    Rails.logger.info("Starting running analyser:")

    bar = RakeProgressbar.new(Report.count)


    ActiveRecord::Base.silence_auto_explain { TextSession.includes(:key_strokes) }
    ActiveRecord::Base.silence_auto_explain { TextSession.includes(:n_grams) }
    KeyStroke.includes(:up_event)

    Report.all.each do |report|
      # KeyStrokeAssignment.analyse(report.text_session,report)
      # NGramGenerator.analyse(report.text_session,report)
      # StrokedTextGenerator.analyse(report.text_session)
      # DetectTextEnding.analyse(report.text_session, report)
      # KeyStrokeCounter.analyse(report.text_session, report)
      # KeyStrokeTimer.analyse(report.text_session, report)
      # CorrectionCounter.analyse(report.text_session, report)
      # CalculateLevenshteinDistance.analyse(report.text_session,report)
      # WordTimer.analyse(report.text_session,report)
      # WordLetterTimer.analyse(report.text_session, report)
      # KeyStrokeNGramTimer.analyse(report.text_session,report)
      KeyStrokeHistogram.analyse(report.text_session,report)
      bar.inc
    end

    bar.finished
    Rails.logger.info("Done running analysers.")
  end


  @logger_initialized = false
  def init_logger
    unless @logger_initialized
      Rails.logger = Log4r::Logger.new("Report Generation")
      Rails.logger.outputters = [Log4r::StdoutOutputter.new('Stdout Logger')]
      Rails.logger.level = 2 # INFO
      Rails.logger.info("Starting report Logger...done.")
    end
  end

end